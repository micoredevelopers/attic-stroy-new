<?php
//
///*
//|--------------------------------------------------------------------------
//| Web Routes
//|--------------------------------------------------------------------------
//|
//| Here is where you can register web routes for your application. These
//| routes are loaded by the RouteServiceProvider within a group which
//| contains the "web" middleware group. Now create something great!
//|
//*/
//
//Route::get('/', function () {
//    return view('welcome');
//});
//
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'IndexController@index')->name('index');
Route::get('/about', 'IndexController@about')->name('about');
Route::get('/building', 'IndexController@building')->name('building');
Route::get('/repair', 'IndexController@repair')->name('repair');
Route::get('/portfolio', 'IndexController@portfolio')->name('portfolio');
Route::get('/portfolio/{id}', 'IndexController@portfolioShow')->name('portfolio.show');
Route::get('/contacts', 'IndexController@contacts')->name('contacts');
Route::post('/contacts', 'IndexController@sendContacts')->name('contact.send');
Route::post('/building', 'IndexController@sendBuilding')->name('building.send');
Route::get('/sitemap.xml', 'IndexController@sitemaps')->name('sitemap');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Auth::routes();
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'IndexController@index')->name('admin.index');

    Route::get('/setting', 'IndexController@index')->name('admin.setting');
    Route::post('/setting', 'IndexController@update')->name('admin.setting.update');

    Route::resource('/contact', 'ContactController', ['as' => 'admin']);
    Route::resource('/portfolio', 'PortfolioController', ['as' => 'admin']);
    Route::get('/portfolio/{id}/hide', 'PortfolioController@hide', ['as' => 'admin'])->name('admin.portfolio.hide');
    Route::post('/portfolio/{id}/position', 'PortfolioController@changePosition', ['as' => 'admin'])->name('admin.portfolio.position');

    Route::match(['get', 'post'],'/comments/create', 'CommentsController@create')->name('admin.comments.create');
    Route::match(['get', 'post'],'/comments/update/{id}', 'CommentsController@update')->name('admin.comments.update');
    Route::get('/comments/delete/{id}', 'CommentsController@delete')->name('admin.comments.delete');
    Route::get('/comments', 'CommentsController@index')->name('admin.comments.index');

});
