<?php
/**
 * Created by PhpStorm.
 * User: aljajazva
 * Date: 2019-04-10
 * Time: 16:39
 */

namespace App\Http\Helpers;


class Helpers
{

    public static function isStorageFileExists($filename, $disk = 'upload')
    {
        return \Storage::disk($disk)->exists($filename);
    }

    public static function imagePath($filename, $disk = 'upload')
    {
        return \Storage::disk($disk)->url($filename);
    }
}