<?php

namespace App\Http\Controllers;


use App\Comments;
use App\Contact;
use App\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class IndexController extends Controller
{

    public function index()
    {

        return view('index');
    }

    public function about()
    {
        return view('about');
    }

    public function building()
    {
        return view('building');
    }

    public function repair()
    {
        $commentsList = Comments::get();
        return view('repair', ['commentsList' => $commentsList]);
    }

    public function portfolio()
    {
        $portfolios_repair = Portfolio::where('status', 1)->where('category', 0)->orderBy('position', 'asc')->get();
        $portfolios_build = Portfolio::where('status', 1)->where('category', 1)->orderBy('position', 'asc')->get();
        return view('portfolio', compact('portfolios_repair', 'portfolios_build'));
    }

    public function portfolioShow($id)
    {
	 $position = Portfolio::select('position')->orderBy('position')->get()->toArray();
	 $arr_pos = array_map(function($item){
	    return $item['position'];
	},$position);
        $portfolio = Portfolio::where('id', $id)->first();
        $key = array_search($portfolio->position, $arr_pos);
        array_key_exists(($key - 1), $arr_pos) ? $position_prev = $arr_pos[$key - 1] : $position_prev = $arr_pos[count($arr_pos) -1];
        array_key_exists(($key + 1), $arr_pos) ? $position_next = $arr_pos[$key + 1] : $position_next = $arr_pos[0];
    
        $portfolio_prev = Portfolio::where('category', $portfolio->category)->where('position', $position_prev)->first();
        $portfolio_next = Portfolio::where('category', $portfolio->category)->where('position', $position_next)->first();


        return view('portfolio-show', compact('portfolio', 'portfolio_prev', 'portfolio_next'));
    }

    public function contacts()
    {
        $contact = Contact::first();

        return view('contacts', compact('contact'));
    }

    public function sendContacts(Request $request)
    {
        $data = $request->except('_token');
        $validator = Validator::make($data, [
            'name' => 'required',
            'phone' => "required|string|min:11",
            //'email' => 'email'
        ]);


        if ($validator->fails()) {
            return response(['status' => 'failure', 'message' => '', 'data' => $validator->errors()->all()]);
        }

        $sendTo = Contact::select('email')->first();


        Mail::send('mailContact', ["request" => $request], function ($message) use ($sendTo) {
		    $message->from(env('MAIL_USERNAME'), 'Contact form');
            $message->to($sendTo->email, 'Contact form')->subject('Contact form');
			//$message->to('oshchyp.denys@gmail.com', 'Contact form')->subject('Contact form');
        });

        if (count(Mail::failures()) > 0)
            return response(['status' => 'failure', 'message' => 'Something wrong', 'data' => Mail::failures()]);
        else
            return response(['status' => 'success', 'message' => '']);

    }

    public function sendBuilding(Request $request)
    {
        $data = $request->except('_token');
        $validator = Validator::make($data, [
            'name' => 'required',
            'phone' => "required|string|min:11"
        ]);


        if ($validator->fails()) {
            return response(['status' => 'failure', 'message' => '', 'data' => $validator->errors()->all()]);
//            return redirect()->back()
//                ->withErrors($validator)
//                ->withInput();
        }
        $sendTo = Contact::select('email')->first();
        Mail::send('mailContact', ["request" => $request], function ($message) use ($sendTo) {
            $message->from(env('MAIL_USERNAME'), 'Contact form');
//            $message->to('katorif@ukr.net', 'Contact form')->subject('Contact form');
            $message->to($sendTo->email, 'Contact form')->subject('Contact form');
        });

        if (count(Mail::failures()) > 0)
            return response(['status' => 'failure', 'message' => 'Something wrong', 'data' => Mail::failures()]);
        else
            return response(['status' => 'success', 'message' => '']);
    }

public function sitemaps()
{
    $portfolios_build = Portfolio::where('status', 1)->orderBy('position', 'asc')->get();

    return response()->view('sitemap', compact('portfolios_repair', 'portfolios_build'))->header('Content-Type', 'text/xml');
}
}