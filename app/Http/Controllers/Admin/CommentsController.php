<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 26.03.2019
 * Time: 17:31
 */

namespace App\Http\Controllers\Admin;

use App\Comments;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{


    public function index(){
        $commentsList = Comments::get();
        return view('admin.comments.index', ['commentsList' => $commentsList]);
    }


    public function create(Comments $comments, Request $request){

      return $this->save($comments, $request);
    }

    public function update($id, Request $request){
        $comments = Comments::findOrFail($id);
        return $this->save($comments, $request);
    }

    public function delete($id){
        $comments = Comments::find($id);
        if ($comments === null){
            abort(404);
        }
        $comments->delete();
        return redirect()->route('admin.comments.index');
    }

    private function save(Comments $comments, Request $request){
        if ($request->isMethod('POST')){
            $data = $request->except('_method', '_token');

            $validator = Validator::make($data, [
                'name' => "required|string",
                'position' => "string",
                'comment' => "required|string",
            ]);

            // dd($data);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $comments->fill($data)->save();

            if ($request->has('image')){
                $ext = $request->image->getClientOriginalExtension();
                $filename = $comments->id . '.' . $ext;
                $imageContent = file_get_contents($request->file('image'));
                $imagePath = 'comments/' . $filename;
                \Storage::disk('upload')->put($imagePath, $imageContent);

                $data['image'] = $imagePath;
            }

            $comments->fill($data)->save();


            return redirect()->route('admin.comments.index');
        }

        return view('admin.comments.save', [
            'comments' => $comments
        ]);
    }

}