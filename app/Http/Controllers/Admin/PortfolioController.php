<?php

namespace App\Http\Controllers\Admin;

use App\Portfolio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Intervention\Image\Constraint;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::orderBy('category')->orderBy('position')->get();
//        $positions = Portfolio::select('position')->orderBy('position')->get();
//        dd($positions[0]['position']);
        return view('admin.portfolios.index', [
            'portfolios' => $portfolios
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $positions = Portfolio::select('position')->get();
        return view('admin.portfolios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $max_position = Portfolio::select('position')->max('position');
        $data = $request->all();

        $validator = Validator::make($data, [
            'main_photo' => 'required|mimes:jpg,jpeg,png',
            'mini_photo' => "required|mimes:jpg,jpeg,png",
            'category' => "required",
            'title' => "required|max:70",
            'desc' => "required|max:302",
            'content' => "required|max:710",
//            'slider_photo' => "required|min:3",
        ]);


        if ($validator->fails()) {
            return redirect('admin/portfolio/create')
                ->withErrors($validator)
                ->withInput();
        }
        //save main photo
        if ($request->hasFile('main_photo')) {

            $image = $this->upload($request->file('main_photo'), Config::get('settings.main_photo_max'));
            if ($image['status'] === true) {
                $main_photo[] = $image['fullFileName'];
            } else {
                return redirect()->back()->withErrors(['image' => $image['status']]);
            }

            $image = $this->upload($request->file('main_photo'), Config::get('settings.main_photo_min'));
            if ($image['status'] === true) {
                $main_photo[] = $image['fullFileName'];
            } else {
                return redirect()->back()->withErrors(['main_photo' => $image['status']]);
            }
            $data['main_photo'] = json_encode($main_photo);
        }

        //save mini photo
        if ($request->hasFile('mini_photo')) {

            $image = $this->upload($request->file('mini_photo'), Config::get('settings.mini_photo'));
            if ($image['status'] === true) {
                $data['mini_photo'] = $image['fullFileName'];
            } else {
                return redirect()->back()->withErrors(['mini_photo' => $image['status']]);
            }
        }

        //Проверка отправил ли пользователь изображение
        if ($request->hasFile('slider_photo')) {
            foreach ($request->slider_photo as $key => $slider) {
                $image = $this->upload($slider, Config::get('settings.slider_photo'));
                if ($image['status'] === true) {
                    $sliders[] = $image['fullFileName'];
                } else {
                    return redirect()->back()->withErrors(['slider_photo' => $image['status']]);
                }
            }
            $data['slider'] = json_encode($sliders);
        }
        $data['position'] = (int)$max_position + 1;
        $portfolio = Portfolio::create($data);

//        if ($request->hasFile('slider_photo')) {
//            foreach ($request->slider_photo as $key => $slider)
//            $portfolio->addMedia($slider)->toMediaCollection('sliders');
//
//        }

        if ($portfolio) return redirect()->route('admin.portfolio.index');
        else  return redirect()->back()->withErrors(['save' => 'something wrong']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolio = Portfolio::find($id);
        return view('admin.portfolios.edit', [
            'portfolio' => $portfolio
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $portfolio = Portfolio::find($id);
        $data = $request->all();
//        if(isset($data['slider']) && ($data['slider'])) {
//            $data['slider'] = array_values(array_diff($data['slider'], $data['del_slider']));
//        }



        $validator = Validator::make($data, [
            'main_photo' => 'required_without:main_present|mimes:jpg,jpeg,png',
            'mini_photo' => "required_without:mini_present|mimes:jpg,jpeg,png",
            'category' => "required",
            'title' => "required|max:70",
            'desc' => "required|max:302",
            'content' => "required|max:710",
//            'slider_photo' => "required_without:slider-0,slider-1,slider-2",
        ]);
        $data['slider'] = $portfolio->slider;

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        
        //Deleting photo
        if ($data['close_main_photo']) Storage::disk('upload')->delete($data['close_main_photo']);
        if ($data['close_mini_photo']) Storage::disk('upload')->delete($data['close_mini_photo']);
        foreach ($data['del_slider'] as $key => $del_slider)
            if ($del_slider) {
                Storage::disk('upload')->delete($del_slider);
                unset($data['slider'][$key]);
            }

            //doing new array after deleting (new index)
//        if ($data['slider'])
//                    dd($data['slider']);
//        $data['slider'] = array_values($data['slider']);

        //save main photo
        if ($request->hasFile('main_photo')) {

            $image = $this->upload($request->file('main_photo'), Config::get('settings.main_photo_max'));
            if ($image['status'] === true) {
                $main_photo[] = $image['fullFileName'];
            } else {
                return redirect()->back()->withErrors(['image' => $image['status']]);
            }

            $image = $this->upload($request->file('main_photo'), Config::get('settings.main_photo_min'));
            if ($image['status'] === true) {
                $main_photo[] = $image['fullFileName'];
            } else {
                return redirect()->back()->withErrors(['main_photo' => $image['status']]);
            }
            $data['main_photo'] = json_encode($main_photo);
        }

        //save mini photo
        if ($request->hasFile('mini_photo')) {

            $image = $this->upload($request->file('mini_photo'), Config::get('settings.mini_photo'));
            if ($image['status'] === true) {
                $data['mini_photo'] = $image['fullFileName'];
            } else {
                return redirect()->back()->withErrors(['mini_photo' => $image['status']]);
            }
        }

//        //Проверка отправил ли пользователь изображение
        if ($request->hasFile('slider_photo')) {
            foreach ($request->slider_photo as $key => $slider) {
                $image = $this->upload($slider, Config::get('settings.slider_photo'));
                if ($image['status'] === true) {
                    $data['slider'][] = $image['fullFileName'];
                } else {
                    return redirect()->back()->withErrors(['slider_photo' => $image['status']]);
                }
            }
        }
        $update_portfolio = $portfolio->update($data);

        if ($update_portfolio) return redirect()->route('admin.portfolio.index');
        return redirect()->back()->withErrors(['save' => 'something wrong']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::find($id);

        $portfolio->delete();

        return redirect()->back();

    }

    public function hide($id)
    {

        $portfolio = Portfolio::find($id);
        ($portfolio->status) ? $portfolio->update(['status' => 0]) : $portfolio->update(['status' => 1]);
        return redirect()->back();
    }

    public function changePosition($id, Request $request)
    {

        $portfolio = Portfolio::find($id);

        if ($portfolio->position != $request->position) {

            $portfolio_old = Portfolio::where('position', $request->position)->first();

            if ($portfolio_old) $portfolio_old->update(['position' => $portfolio->position]);

            $portfolio->update(['position' => $request->position]);

        }


        return redirect()->back();
    }


    public function upload($file, $size_path)
    {
        $path = 'portfolio' . '/' . date('F') . date('Y') . '/';

        $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk('upload')->exists($path . $filename . '.' . $file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()) . (string)($filename_counter++);
        }

        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();


        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png'])) {
            $image = Image::make($file)
                ->resize($size_path['width'], $size_path['height'], function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->encode($file->getClientOriginalExtension(), 75);

            // move uploaded file from temp to uploads directory
            if (Storage::disk('upload')->put($fullPath, (string)$image, 'public')) {
                $status = true;
                $fullFilename = $fullPath;
            } else {
                $status = 'Ошибка загрузки изображения';
            }
        } else {
            $status = 'Неправильный тип изображения';
        }

        // echo out script that TinyMCE can handle and update the image in the editor
        return ['status' => $status, 'fullFileName' => $fullFilename];
    }
}
