<?php

namespace App\Providers;

use App\Contact;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    public $formats = array(
        '12' => '+############', // for +38 0XX XX XXX XX or 38 0XX XX XXX XX
        '10' => '+38##########' // for 0XX XX XXX XX
    );

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);


//        Validator::extend('phone', function ($attribute, $value, $parameters, $validator) {
//            return preg_match('%^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$%i', $value) && strlen($value) >= 10;
//        });
//
//        Validator::replacer('phone', function ($message, $attribute, $rule, $parameters) {
//            return str_replace(':attribute', $attribute, ':attribute is invalid phone number');
//        });

        $this->contacts();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function contacts()
    {
        $contact = Contact::first();
        $tel_1 = $this->PhoneFormat($contact->phone_1, $this->formats, '#');
        $tel_2 = $this->PhoneFormat($contact->phone_2, $this->formats, '#');
        View::composer('layouts.app', function ($view) use($contact, $tel_1, $tel_2) {
            $view->with(['contact' => $contact, 'tel_1' => $tel_1, 'tel_2' => $tel_2]);
        });
    }

    public function PhoneFormat($phone, $format, $mask = '#', $codeSplitter = '0')
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);

        $phone = substr($phone, strpos($phone, $codeSplitter));

        if (is_array($format)) {
            if (array_key_exists(strlen($phone), $format)) {
                $format = $format[strlen($phone)];
            } else {
                return $phone;
            }
        }

        $pattern = '/' . str_repeat('([0-9])?', substr_count($format, $mask)) . '(.*)/';

        $format = preg_replace_callback(
            str_replace('#', $mask, '/([#])/'),
            function () use (&$counter) {
                return '${' . (++$counter) . '}';
            },
            $format
        );

        return ($phone) ? trim(preg_replace($pattern, $format, $phone, 1)) : false;
    }


}
