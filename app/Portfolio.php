<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use Spatie\MediaLibrary\Conversion\Conversion;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;

class Portfolio extends Model
{
    protected $fillable = [
        'category',
        'main_photo',
        'mini_photo',
        'slider',
        'title',
        'desc',
        'content',
        'position',
        'status'
    ];

    public $casts = [
        'slider' => 'array',
    ];

    public static function boot() {
        parent::boot();

        static::deleting(function($portfolio) { // before delete() method call this

            if ($portfolio->main_photo) {
                foreach (json_decode($portfolio->main_photo) as $image)
                    if ($image) {
                        Storage::disk('upload')->delete($image);
                    }
            }

            if ($portfolio->mini_photo) {
                Storage::disk('upload')->delete($portfolio->mini_photo);
            }

            if ($portfolio->slider) {
                foreach (json_decode($portfolio->slider) as $image)
                    if ($image) {
                        Storage::disk('upload')->delete($image);
                    }
            }

        });

    }

    /**
     * Set the polymorphic relation.
     *
     * @return mixed
     */
    public function media()
    {
        // TODO: Implement media() method.
    }

    /**
     * Move a file to the medialibrary.
     *
     * @param string|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return \Spatie\MediaLibrary\FileAdder\FileAdder
     */
    public function addMedia($file)
    {
        // TODO: Implement addMedia() method.
    }

    /**
     * Copy a file to the medialibrary.
     *
     * @param string|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return \Spatie\MediaLibrary\FileAdder\FileAdder
     */
    public function copyMedia($file)
    {
        // TODO: Implement copyMedia() method.
    }

    /**
     * Determine if there is media in the given collection.
     *
     * @param $collectionMedia
     *
     * @return bool
     */
    public function hasMedia(string $collectionMedia = ''): bool
    {
        // TODO: Implement hasMedia() method.
    }

    /**
     * Get media collection by its collectionName.
     *
     * @param string $collectionName
     * @param array|callable $filters
     *
     * @return \Illuminate\Support\Collection
     */
    public function getMedia(string $collectionName = 'default', $filters = [])
    {
        // TODO: Implement getMedia() method.
    }

    /**
     * Remove all media in the given collection.
     *
     * @param string $collectionName
     */
    public function clearMediaCollection(string $collectionName = 'default')
    {
        // TODO: Implement clearMediaCollection() method.
    }

    /**
     * Remove all media in the given collection except some.
     *
     * @param string $collectionName
     * @param \Spatie\MediaLibrary\Media[]|\Illuminate\Support\Collection $excludedMedia
     *
     * @return string $collectionName
     */
    public function clearMediaCollectionExcept(string $collectionName = 'default', $excludedMedia = [])
    {
        // TODO: Implement clearMediaCollectionExcept() method.
    }

    /**
     * Determines if the media files should be preserved when the media object gets deleted.
     *
     * @return bool
     */
    public function shouldDeletePreservingMedia()
    {
        // TODO: Implement shouldDeletePreservingMedia() method.
    }

    /**
     * Cache the media on the object.
     *
     * @param string $collectionName
     *
     * @return mixed
     */
    public function loadMedia(string $collectionName)
    {
        // TODO: Implement loadMedia() method.
    }

    public function addMediaConversion(string $name): Conversion
    {
        // TODO: Implement addMediaConversion() method.
    }

    public function registerMediaConversions(Media $media = null)
    {
        // TODO: Implement registerMediaConversions() method.
    }

    public function registerMediaCollections()
    {
        // TODO: Implement registerMediaCollections() method.
    }

    public function registerAllMediaConversions()
    {
        // TODO: Implement registerAllMediaConversions() method.
    }
}
