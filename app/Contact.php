<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'phone_1',
        'phone_2',
        'email',
        'address',
        'map',
        'link_f',
        'link_i'
    ];

}
