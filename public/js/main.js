/******/ (function (modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if (installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
			/******/
}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
			/******/
};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
		/******/
}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function (exports, name, getter) {
/******/ 		if (!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
	/******/
});
			/******/
}
		/******/
};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function (module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
		/******/
};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function (object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
	/******/
})
/************************************************************************/
/******/([
/* 0 */
/***/ (function (module, exports, __webpack_require__) {

		module.exports = __webpack_require__(1);


		/***/
}),
/* 1 */
/***/ (function (module, exports) {

		var pageId = $('main').attr('id');

		function initPage() {
			var IS_ANIMATED_BURGER = false;

			function handleSendMail(e) {
				var $that = $(this);
				var toggle = false;
				e.preventDefault();
				var HEADERS = {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				};

				$that.find('input[type=tel], input[type=name]').each((index, item) => {
				    if ($(item).val() === '') {
                        $that.find('.error_text').css('display', 'block');
                        $that.find('.error_text .text').html(`
                            <p>Поле ${$(item).attr('placeholder')} обязательно к заполнению!</p>
                        `);
                    } else {
                        toggle = true;
                    }
                });

				if (toggle === true) {
                    $.ajax({
                        url: $(this).attr('action'),
                        type: 'post',
                        headers: HEADERS,
                        data: new FormData(this),
                        contentType: false,
                        processData: false
                    }).then(function (res) {
                        if (res.status === 'success') {
                            $('#feedback_modal').modal('hide');
                            $('#success_modal').modal('show');
                            $that.find('input').val('');
                        } else {
                            $that.find('.error_text').css('display', 'block');
                            $that.find('.error_text .text').html(res.data);
                        }
                    });
                }
			}

			$(document).ready(function () {
				$('.contact_send').on('submit', handleSendMail);
				$('input[type$=tel], input.number').mask("(999) 999-99-99");
			});

			$(window).scroll(function () {
				var scrollBottom = $(window).scrollTop() + $(window).height();
				var footer = $('footer').position().top;
				if ($(window).scrollTop() > 40) {
					$('header').addClass('active');
				} else {
					$('header').removeClass('active');
				}
				if (scrollBottom > footer) {
					$('footer').addClass('active');
				}
			});

			$(window).on('load', function () {
				$('header').addClass('show');
				if ($(window).scrollTop() > 40) {
					$('header').addClass('active');
				} else {
					$('header').removeClass('active');
				}
			});

			$('.menu_icon').click(function () {
				$('body').toggleClass('isOver pageLoad');
				// $('header').addClass('active');
				if (IS_ANIMATED_BURGER) return;
				IS_ANIMATED_BURGER = true;

				if ($(this).parents('.header_container').hasClass('black')) {
					$('.header_container.desktop.white').addClass('blockHide');
					IS_ANIMATED_BURGER = false;
				}

				if ($(window).height() < 540) {
					$('.menu_container').toggleClass('isRollUp');
				}

				if ($('.menu_container').hasClass('isRollUp')) {
					$('header').css('background-color', '#fff');
				} else {
					$('header').css('background-color', 'transparent');
				}

				$('.menu_container').toggleClass('active');
				if ($(this).hasClass('isChecked')) {
					$('.menu_icon').removeClass('isAnimatedThird');
					setTimeout(function () {
						$('.menu_icon').removeClass('isAnimatedSecond');
					}, 300);
					setTimeout(function () {
						$('.menu_icon').removeClass('isAnimatedFirst');
					}, 600);
					setTimeout(function () {
						$('.menu_icon').removeClass('isChecked');
						IS_ANIMATED_BURGER = false;
					}, 900);
				} else {
					$('.menu_icon').addClass('isAnimatedFirst');
					setTimeout(function () {
						$('.menu_icon').addClass('isAnimatedSecond');
					}, 300);
					setTimeout(function () {
						$('.menu_icon').addClass('isAnimatedThird');
					}, 600);
					setTimeout(function () {
						$('.menu_icon').addClass('isChecked');
						IS_ANIMATED_BURGER = false;
					}, 900);
				}
				$('.header_container.desktop .menu_icon').addClass('isDisabled');
				setTimeout(function () {
					$('.header_container.desktop .menu_icon').removeClass('isDisabled');
				}, 800);
			});

			$('.show_more').click(function () {
				$(this).find('.my_btn').addClass('isHidden');
				setTimeout(function () {
					$('.show_more').css('display', 'none');
					$('.text').addClass('fullShow');
				}, 300);
			});
            $('.show_more_home').click(function () {
				setTimeout(function () {
					$('.front-seo').css('z-index', '999');
					$('.show_more_home').css('z-index', '-1');
				}, 300);
			});
            $('.front-seo .close').click(function () {
				
				setTimeout(function () {
					$('.front-seo').css('z-index', '-1');
					$('.show_more_home').css('z-index', '9');
				}, 300);
			});

			$('.swiper-button-next, .swiper-button-prev').click(function () {
				$(this).parents('.swiper-container-horizontal').find('.swiper-slide').addClass('active');
				setTimeout(function () {
					$('.swiper-slide').removeClass('active');
				}, 800);
			});
			var wow = new WOW({
				boxClass: 'wow',
				animateClass: 'animated',
				offset: 0,
				mobile: true,
				live: true
			});
			wow.init();
		}

		initPage();

		switch (pageId) {
			case 'wrapper_main':
				{
					initMainPage();
					break;
				}
			case 'wrapper_contacts':
				{
					initContactsPage();
					break;
				}
			case 'wrapper_about':
				{
					initAboutPage();
					break;
				}
			case 'wrapper_portfolio':
				{
					initPortfolioPage();
					break;
				}
			case 'wrapper_selected_portfolio':
				{
					initSelectedPortfolioPage();
					break;
				}
			case 'wrapper_repair':
				{
					initRepairPage();
					break;
				}
			case 'wrapper_building':
				{
					initBuildingPage();
					break;
				}
		}

		function initMainPage() {
			var $leftBlock = $('.main_block.left');
			var $rightBlock = $('.main_block.right');

			$('.header_container.mobile.white').addClass('blockHide');
			$('.header_container.desktop.black').addClass('blockHide');

			$(window).on('load', function () {
				if ($(window).width() < 1199) {
					$('header').css('background-color', '#fff');
				}
				if ($(window).height() < 540) {
					$('#wrapper_main').addClass('isRollUp');
				}
				setTimeout(function () {
					$('header').addClass('show');
				});
				$('#wrapper_main').addClass('isLoaded');
				$('.main_block').addClass('active');
				$('.main_block .skew_line').addClass('active');
				setTimeout(function () {
					$('.main_block.right').css('pointer-events', 'visible');
				}, 1500);
			});

			$('.menu_icon').click(function () {
				if ($(this).parents('.header_container').hasClass('white')) {
					$(this).parents('.header_container').addClass('blockHide');
					$('.header_container.desktop.black').removeClass('blockHide');
				} else {
					setTimeout(function () {
						$('.header_container.desktop.black').addClass('blockHide');
						$('.header_container.desktop.white').removeClass('blockHide');
					}, 400);
				}
				$('.header_container.desktop .menu_icon').addClass('isDisabled');
				setTimeout(function () {
					$('.header_container.desktop .menu_icon').removeClass('isDisabled');
				}, 800);
			});

			$rightBlock.addClass('isRollUp');
			$leftBlock.hover(function () {
				$leftBlock.css('width', '85%');
				$rightBlock.css('width', '15%');
				$rightBlock.find('.skew_line').css({
					'right': '-40%',
					'width': '185%'
				});
				$rightBlock.addClass('isRollUp');
				$leftBlock.removeClass('isRollUp');
			});
			$rightBlock.hover(function () {
				$rightBlock.css('width', '65%');
				$leftBlock.css('width', '35%');
				$rightBlock.find('.skew_line').css({
					'right': '-10%',
					'width': '120%'
				});
				$leftBlock.addClass('isRollUp');
				$rightBlock.removeClass('isRollUp');
			});
		}

		function initContactsPage() {
			const checkWindHeight = () => {
				const $contactCont = $('#wrapper_contacts');
				if ($(window).height() <= 500) {
					$contactCont.addClass('isMinHeight');
				} else {
					$contactCont.removeClass('isMinHeight');
				}
			}
			$(document).ready(checkWindHeight);
			$(window).resize(checkWindHeight);
			$('.header_container.mobile.white').addClass('blockHide');
			$('.header_container.desktop.white').addClass('blockHide');
			$(window).on('load', function () {
				var element = document.getElementById('number');
				var maskOptions = {
					mask: '+{38}(000)000-00-00'
				};
				var mask = new IMask(element, maskOptions);
				$('.form-group, .custom_btn').addClass('active');
				$('.request_button').click(function () {
					for (var i = 0; i < $('.form-group').length; i++) {
						var $input = $($('.form-group')[i]).find('input');
						if ($('input.number').val().length < 13 && $input.val() === '') {
							$input.addClass('isError');
						} else {
							$input.removeClass('isError');
						}
					}
				});
			});
		}

		function initAboutPage() {
			var SWIPE_TIME_OUT = null;

			$('.header_container.mobile.white').addClass('blockHide');
			$('.header_container.desktop.white').addClass('blockHide');

			$(window).click(function() {
				if ($('body').hasClass('modal-open')) {
					$('#wrapper_all').toggleClass('over');
				}
			});

			var mySwiper = new Swiper('.about_slider', {
				slidesPerView: 1,
				spaceBetween: 30,
				loop: true,
				noSwiping: true,
				effect: 'fade',
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function renderBullet(index, className) {
						return '<span class="' + className + '"></span>';
					}
				}
			});

			function changeSlideNext() {
				mySwiper.slideNext();
			}
			function changeSlidePrev() {
				mySwiper.slidePrev();
			}

			$('.swiper-button-next').click(function () {
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlideNext, 800);
			});

			$('.swiper-button-prev').click(function () {
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlidePrev, 800);
			});

			$('.top_row').click(function () {
				$(this).find('.icon_cont').toggleClass('rotate');
				$(this).parent().find('.bottom_row').toggleClass('isHidden');
			});

			$(window).scroll(function () {
				var scrollBottom = $(window).scrollTop() + $(window).height();
				var slider = $('.about_slider').position().top;
				var footer = $('footer').position().top;
				if (scrollBottom > slider) {
					$('.swiper-slide .image_container').addClass('active');
				}
				if (scrollBottom > footer) {
					$('footer').addClass('active');
				}
			});
		}

		function initPortfolioPage() {
			$('header').addClass('black');
			$('.header_container.mobile.black').addClass('blockHide');
			$('.header_container.desktop.black').addClass('blockHide');

			$(window).on('load', function () {
				$('.section_top_info').addClass('active');
			});

			$(window).scroll(function () {
				if ($(window).scrollTop() > 40) {
					$('.header_container.white').addClass('blockHide');
					$('.header_container.black').removeClass('blockHide');
				} else {
					$('.header_container.white').removeClass('blockHide');
					$('.header_container.black').addClass('blockHide');
				}
			});

			$('.custom_btn').click(function () {
				$('.custom_btn').removeClass('isShow');
				if ($(this).hasClass('isShow')) {
					$(this).removeClass('isShow');
				} else {
					$(this).addClass('isShow');
				}

				for (var i = 0; i < $('.block_row').length; i++) {
					var $blockElem = $($('.block_row')[i]);
					if ($(this).data('id') === $blockElem.attr('id')) {
						$('.block_row').removeClass('isShow');
						if ($blockElem.hasClass('isShow')) {
							$blockElem.removeClass('isShow');
						} else {
							$blockElem.addClass('isShow');
						}
					}
				}
			});

			$('.menu_icon').click(function () {
				if ($(this).parents('header').hasClass('active') && $(this).parents('.header_container.mobile').hasClass('black')) {
				    $(this).parents('.header_container').removeClass('blockHide');
				    $('.header_container.mobile.white').addClass('blockHide');
				} else if ($(this).parents('.header_container.mobile').hasClass('white')) {
				    $(this).parents('.header_container').addClass('blockHide');
					$('.header_container.mobile.black').removeClass('blockHide');
				} else if ($(this).parents('.header_container.mobile').hasClass('black') && !$(this).parents('header').hasClass('active')) {
				    setTimeout(function () {
						$('.header_container.mobile.black').addClass('blockHide');
						$('.header_container.mobile.white').removeClass('blockHide');
					}, 400);
				}
				if ($(this).parents('.header_container.desktop').hasClass('white')) {
					$(this).parents('.header_container').addClass('blockHide');
					$('.header_container.desktop.black').removeClass('blockHide');
				} else if ($(this).parents('header').hasClass('active')) {
					setTimeout(function () {
						$('.header_container.desktop.black').removeClass('blockHide');
						$('.header_container.desktop.white').addClass('blockHide');
					}, 400);
				} else {
					setTimeout(function () {
						$('.header_container.desktop.black').addClass('blockHide');
						$('.header_container.desktop.white').removeClass('blockHide');
					}, 400);
				}
				$('.header_container.desktop .menu_icon').addClass('isDisabled');
				setTimeout(function () {
					$('.header_container.desktop .menu_icon').removeClass('isDisabled');
				}, 800);
			});
		}

		function initSelectedPortfolioPage() {
			var SWIPE_TIME_OUT = null;

			function onElemScroll() {
				var scrollBottom = $(window).scrollTop() + $(window).height();
				var slider = $('.selected-portfolio-slider').position().top;
				var feedback = $('.feedback_block').position().top;
				var navBlock = $('.works_navigation').position().top;
				if (scrollBottom > slider) {
					$('.selected-portfolio-slider .image_container').addClass('active');
				}
				if (scrollBottom > feedback) {
					$('.feedback_block .image_container').addClass('active');
				}
				if (scrollBottom > navBlock) {
					$('.works_navigation .work_block').addClass('active');
				}
			}

			$('.header_container.mobile.white').addClass('blockHide');
			$('.header_container.desktop.white').addClass('blockHide');

			setTimeout(function () {
				$('.portfolio_info').addClass('active');
			}, 300);
			setTimeout(function () {
				$('.portfolio_info .image_container').addClass('active');
			}, 450);

			var mySwiper = new Swiper('.selected-portfolio-slider', {
				slidesPerView: 1,
				loop: true,
				speed: 400,
				effect: 'fade',
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function (index, className) {
						
						return '<span class="' + className + '"></span>';
					}
				}
			});

			function changeSlideNext() {
				mySwiper.slideNext();
			}
			function changeSlidePrev() {
				mySwiper.slidePrev();
			}

			$('.swiper-button-next').click(function () {
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlideNext, 600);
			});

			$('.swiper-button-prev').click(function () {
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlidePrev, 600);
			});

			$(window).scroll(onElemScroll);
		}

		function initRepairPage() {

			$('.images_row .image_container img').on('click', function () {
				const $modalCont = $('#myModal');
				$modalCont.css('display', 'flex');
				$("#img01").attr('src', $(this).attr('src'));
			});
			$('#myModal').on('click', function () {
				const $modalCont = $('#myModal');
				$modalCont.css('display', 'none');
			});
			$('.close').on('click', function () {
				const $modalCont = $('#myModal');
				$modalCont.css('display', 'none');
			});

			var isChecked = false;
			var SWIPE_TIME_OUT = null;

			//$('.phone.black').css('display', 'none');

			// $('[data-toggle="tooltip"]').tooltip();

			$(document).ready(function() {
				if ($(window).height() < 540) {
					$('.main_page').addClass('isRollUp');
				}
			});

			$(window).scroll(function () {/*
				if ($(window).scrollTop() > 40) {
					$('.phone.black').css('display', 'block');
					$('.phone.white').css('display', 'none');
				} else {
					$('.phone.black').css('display', 'none');
					$('.phone.white').css('display', 'block');
				}*/
			});

			function onElemScroll() {
				var scrollBottom = $(window).scrollTop() + $(window).height();
				var descImg = $('.description_section').position().top - 120;
				var feedback = $('.feedback_section').position().top - 120;
				var material = $('.material_info').position().top - 120;
				var objectCalc = $('.object_calc').position().top - 120;
				var review = $('.review_slider').position().top - 120;
				
				if (scrollBottom > descImg) {
					$('.description_section .image_container').addClass('active');
				}
				if (scrollBottom > feedback) {
					$('.feedback_section').addClass('active');
				}
				if (scrollBottom > material) {
					$('.material_info').addClass('active');
					$('.material_info .image_container').addClass('active');
				}
				if (scrollBottom > objectCalc) {
					if (!isChecked) {
						$('.object_calc .top_info .image_container').addClass('active');
						$('.object_calc .top_info .image_container').addClass('show');
						setTimeout(function () {
							$('.object_calc .top_info .image_container').removeClass('active');
						}, 800);
						isChecked = true;
					}
				}
				if (scrollBottom > review) {
					$('.review_slider .bottom_info').addClass('active');
					$('.review_slider .review_user_info').addClass('active');
					$('.review_slider').addClass('active');
				}
			}

			// var bar = new ProgressBar.Circle('#progress-circle', {
			// 	strokeWidth: 3,
			// 	easing: 'easeInOut',
			// 	color: '#ffffff',
			// 	trailColor: '#515151',
			// 	trailWidth: 3,
			// 	svgStyle: null
			// });

			// let startProgress = () => {
			// 	bar.animate(1, {
			// 		duration: 6000
			// 	});
			// }

			let stopProgress = () => {
				bar.animate(0);
			}

			let mySwiper1 = new Swiper('.gallery_slider', {
				slidesPerView: 1,
				loop: true,
				spaceBetween: 15,
				effect: 'fade',
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function renderBullet(index, className) {
						return '<span class="' + className + '"></span>';
					}
				}
			});

			let mySwiper2 = new Swiper('.review_slider', {
				slidesPerView: 1,
				loop: true,
				speed: 600,
				effect: 'fade',
				// autoplay: {
				// 	delay: 6000
				// },
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function renderBullet(index, className) {
						return '<span class="' + className + '">' + (index + 1) + '</span>';
					}
				},
				// on: {
				// 	autoplayStart: function autoplayStart() {
				// 		startProgress();
				// 	},
				// 	autoplay: function autoplay() {
				// 		stopProgress();
				// 		setTimeout(startProgress, 800);
				// 	},
				// 	slideChange: function slideChange() {
				// 		stopProgress();
				// 	}
				// }
			});

			function changeSlideNext1() {
				mySwiper1.slideNext();
			}
			function changeSlidePrev1() {
				mySwiper1.slidePrev();
			}

			function changeSlideNext2() {
				mySwiper2.slideNext();
				// mySwiper2.autoplay.start();
			}
			function changeSlidePrev2() {
				mySwiper2.slidePrev();
				// mySwiper2.autoplay.start();
			}

			$('.gallery_slider .swiper-button-next').click(function () {
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlideNext1, 800);
			});
			$('.gallery_slider .swiper-button-prev').click(function () {
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlidePrev1, 800);
			});

			$('.review_slider .swiper-button-next').click(function () {
				// stopProgress();
				// setTimeout(mySwiper2.autoplay.start(), 800);
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlideNext2, 600);
			});
			$('.review_slider .swiper-button-prev').click(function () {
				// stopProgress();
				// setTimeout(mySwiper2.autoplay.start(), 800);
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlidePrev2, 600);
			});

			$(window).scroll(function () {
				if ($(window).scrollTop() > 40) {
					$('.header_container.half').addClass('onBlack');
				} else {
					$('.header_container.half').removeClass('onBlack');
				}
				onElemScroll();
			});

			$('.menu_icon').click(function () {
				if ($(this).parents('header').hasClass('active')) {
					$(this).parents('.header_container.half').addClass('onBlack');
				} else if (!$(this).parents('header').hasClass('active')) {
					$(this).parents('.header_container.half').toggleClass('onBlack');
				} else {
					$(this).parents('.header_container.half').removeClass('onBlack');
				}
			});

		// 	const imgArrs = [
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/pillow1.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/pillow2.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/pillow3.png',
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/pillow4.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/pillow5.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/pillow6.png',
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/puf.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/puf1.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/puf3.png',
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/sofa.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/sofa1.png',
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/armchair_gray.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/chair-blue.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/chair.png',
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/floor-lamp.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/hni-lamp.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/jac-blue.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/kitchen.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/lamp-on.png',
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/lamp.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/pachwork-chair.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/table-white.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/tov-table-2.png',
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/white.png', 
		// 		'http://dev.comnd-x.com/attic-stroy/images/repair/canvas-img/yellow-chair.png',
		// 	];

		// 	function rand (min, max) {
		// 		return Math.floor(Math.random() * (max - min + 1)) + min;
		// 	}

		// 	var ctx = document.getElementById('canvasRegn').getContext('2d'),
		// 		RAD = Math.PI/180,
		// 		width  = window.innerWidth,
		// 		height = window.innerHeight,
		// 		maxImgs = 5,
		// 		fallingIconsAll = [],
		// 		FallingIcon = function() {
		// 			this.x = rand(0, width);
		// 			this.y = rand(0, height);
		// 			this.h = 100;
		// 			this.w = 100;
		// 			this.angle = rand(0, 180);
		// 			this.speed = rand(1, 4);
		// 			this.IMG = new Image();
		// 			for (let i = 0; i < maxImgs; i++) {
		// 				var randImg = imgArrs[Math.floor(Math.random() * imgArrs.length)];
		// 				this.IMG.src = randImg;
		// 			}
		// 			this.IMG.width = 200;
		// 			this.IMG.height = 200;
		// 			return this;
		// 		};
			
		// 	ctx.canvas.width = width,
		// 	ctx.canvas.height = height,

		// 	FallingIcon.prototype.move = function () {
		// 		if (this.y > height + this.h) {
		// 			this.y = -this.h;
		// 			this.x = rand(0, width);
		// 			this.speed = rand(1, 4);
		// 			for (let i = 0; i < maxImgs; i++) {
		// 				var randImg = imgArrs[Math.floor(Math.random() * imgArrs.length)];
		// 				this.IMG.src = randImg;
		// 			}
		// 		}
		// 		this.y += this.speed;
		// 		this.angle += 0.35;

		// 		ctx.save();
		// 		ctx.translate(this.x, this.y);
		// 		ctx.rotate(this.angle * RAD);
		// 		ctx.drawImage(this.IMG, -(this.IMG.width/2), -(this.IMG.height/2), this.IMG.width, this.IMG.height);
		// 		ctx.restore();
		// 	};

		// 	// Create falling Icons
		// 	for (var i = 0; i < maxImgs; i++) {
		// 		fallingIconsAll.push(new FallingIcon());
		// 	}

		// 	// Setup Canvas
		// 	ctx.canvas.width  = width;
		// 	ctx.canvas.height = height;

		// 	// Animation loop
		// 	(function loop () { 
		// 		ctx.clearRect(0, 0, width, height); 
		// 		fallingIconsAll.forEach(function (Icon) {
		// 			Icon.move();
		// 		});
		// 		requestAnimationFrame(loop);
		// 	}());

		// }
		}

		function initBuildingPage() {
			var SWIPE_TIME_OUT = null;

			var isChecked = false;
			var isCheckedWorks = false;

			$('input[type$=tel], .input_cont.number').mask("(999) 999-99-99");

			function onElemScroll() {
				var scrollBottom = $(window).scrollTop() + $(window).height();
				var works = $('.building_slider').position().top - 120;
				var gallery = $('.slide_cont').position().top - 120;
				var objectCalc = $('.object_calc').position().top - 120;
				var review = $('.review_slider').position().top - 120;
				if (scrollBottom > works) {
					if (!isCheckedWorks) {
						$('.building_slider .image_container').addClass('active');
						$('.building_slider .image_container').addClass('isShow');
						setTimeout(function () {
							$('.building_slider .image_container').removeClass('active');
						}, 800);
						isCheckedWorks = true;
					}
				}
				if (scrollBottom > gallery) {
					$('.gallery_slider .image_container').addClass('active');
					$('.gallery_slider .info_container').addClass('active');
				}
				if (scrollBottom > objectCalc) {
					if (!isChecked) {
						$('.object_calc .top_info .image_container').addClass('active');
						$('.object_calc .top_info .image_container').addClass('show');
						setTimeout(function () {
							$('.object_calc .top_info .image_container').removeClass('active');
						}, 800);
						isChecked = true;
					}
				}
				if (scrollBottom > review) {
					$('.review_slider .bottom_info').addClass('active');
					$('.review_slider .review_user_info').addClass('active');
					$('.review_slider').addClass('active');
				}
			}

			$(window).scroll(function () {
				onElemScroll();
			});

			$('.header_container.mobile.white').addClass('blockHide');
			$('.header_container.desktop.white').addClass('blockHide');

			// let bar = new ProgressBar.Circle('#progress-circle', {
			// 	strokeWidth: 3,
			// 	easing: 'easeInOut',
			// 	color: '#ffffff',
			// 	trailColor: '#515151',
			// 	trailWidth: 3,
			// 	svgStyle: null
			// });

			// let barSlide = new ProgressBar.Circle('.progress-slide', {
			// 	strokeWidth: 3,
			// 	easing: 'easeInOut',
			// 	color: '#ffffff',
			// 	trailColor: '#515151',
			// 	trailWidth: 3,
			// 	svgStyle: null
			// });

			// let startProgressBar = () => {
			// 	barSlide.animate(1, {
			// 		duration: 6000
			// 	});
			// }

			// let stopProgressBar = () => {
			// 	barSlide.animate(0);
			// }

			// let startProgress = () => {
			// 	bar.animate(1, {
			// 		duration: 6000
			// 	});
			// }

			// let stopProgress = () => {
			// 	bar.animate(0);
			// }

			let mySwiper1 = new Swiper('.gallery_slider', {
				slidesPerView: 1,
				loop: true,
				spaceBetween: 15,
				speed: 400,
				effect: 'fade',
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function renderBullet(index, className) {
						return '<span class="' + className + '"></span>';
					}
				}
			});

			let mySwiper2 = new Swiper('.review_slider', {
				slidesPerView: 1,
				loop: true,
				speed: 600,
				effect: 'fade',
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function renderBullet(index, className) {
						return '<span class="' + className + '">' + (index + 1) + '</span>';
					}
				},
			});

			var mySwiper3 = new Swiper('#foundation', {
				slidesPerView: 1,
				loop: true,
				speed: 600,
				effect: 'fade',
				noSwiping: true,
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function renderBullet(index, className) {
						return '<span class="' + className + '">' + (index + 1) + '</span>';
					}
				},
				breakpoints: {
					1199: {
						noSwiping: false,
					}
				}
			});

			var mySwiper4 = new Swiper('#houses', {
				slidesPerView: 1,
				loop: true,
				speed: 600,
				effect: 'fade',
				noSwiping: true,
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function renderBullet(index, className) {
						return '<span class="' + className + '">' + (index + 1) + '</span>';
					}
				},
				breakpoints: {
					1199: {
						noSwiping: false,
					}
				}
			});
			
			var mySwiper5 = new Swiper('#landscape', {
				slidesPerView: 1,
				loop: true,
				speed: 600,
				effect: 'fade',
				noSwiping: true,
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function renderBullet(index, className) {
						return '<span class="' + className + '">' + (index + 1) + '</span>';
					}
				},
				breakpoints: {
					1199: {
						noSwiping: false,
					}
				}
			});

			function changeSlideNext1() {
				mySwiper1.slideNext();
			}
			function changeSlidePrev1() {
				mySwiper1.slidePrev();
			}

			function changeSlideNext2() {
				mySwiper2.slideNext();
				// mySwiper2.autoplay.start();
			}
			function changeSlidePrev2() {
				mySwiper2.slidePrev();
				// mySwiper2.autoplay.start();
			}

			function changeSlideNext3() {
				mySwiper3.slideNext();
				// mySwiper3.autoplay.start();
			}
			function changeSlidePrev3() {
				mySwiper3.slidePrev();
				// mySwiper3.autoplay.start();
			}

			function changeSlideNext4() {
				mySwiper4.slideNext();
				// mySwiper4.autoplay.start();
			}
			function changeSlidePrev4() {
				mySwiper4.slidePrev();
				// mySwiper4.autoplay.start();
			}

			function changeSlideNext5() {
				mySwiper5.slideNext();
				// mySwiper5.autoplay.start();
			}
			function changeSlidePrev5() {
				mySwiper5.slidePrev();
				// mySwiper5.autoplay.start();
			}

			$('.gallery_slider .swiper-button-next').click(function () {
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlideNext1, 800);
			});
			$('.gallery_slider .swiper-button-prev').click(function () {
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlidePrev1, 800);
			});

			$('.review_slider .swiper-button-next').click(function () {
				// stopProgress();
				// setTimeout(mySwiper2.autoplay.start(), 800);
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlideNext2, 600);
			});
			$('.review_slider .swiper-button-prev').click(function () {
				// stopProgress();
				// setTimeout(mySwiper2.autoplay.start(), 800);
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlidePrev2, 600);
			});

			$('#foundation .swiper-button-next').click(function () {
				// stopProgressBar();
				// setTimeout(mySwiper3.autoplay.start(), 800);
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlideNext3, 800);
			});
			$('#foundation .swiper-button-prev').click(function () {
				// stopProgressBar();
				// setTimeout(mySwiper3.autoplay.start(), 800);
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlidePrev3, 800);
			});

			$('#houses .swiper-button-next').click(function () {
				// stopProgressBar();
				// setTimeout(mySwiper4.autoplay.start(), 800);
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlideNext4, 800);
			});
			$('#houses .swiper-button-prev').click(function () {
				// stopProgressBar();
				// setTimeout(mySwiper4.autoplay.start(), 800);
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlidePrev4, 800);
			});

			$('#landscape .swiper-button-next').click(function () {
				// stopProgressBar();
				// setTimeout(mySwiper5.autoplay.start(), 800);
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlideNext5, 800);
			});
			$('#landscape .swiper-button-prev').click(function () {
				// stopProgressBar();
				// setTimeout(mySwiper5.autoplay.start(), 800);
				clearTimeout(SWIPE_TIME_OUT);
				SWIPE_TIME_OUT = setTimeout(changeSlidePrev5, 800);
			});

			// $('#progress-circle').click(function () {
			// 	mySwiper2.autoplay.start();
			// 	stopProgress();
			// 	if ($(this).hasClass('isStopped')) {
			// 		$(this).removeClass('isStopped');
			// 		mySwiper2.autoplay.stop();
			// 		startProgress();
			// 	} else {
			// 		$(this).addClass('isStopped');
			// 	}
			// });

			// $('.progress-slide').click(function () {
			// 	mySwiper3.autoplay.stop();
			// 	mySwiper4.autoplay.stop();
			// 	mySwiper5.autoplay.stop();
			// 	stopProgress();
			// 	if ($(this).hasClass('isStopped')) {
			// 		$(this).removeClass('isStopped');
			// 		mySwiper3.autoplay.stop();
			// 		mySwiper4.autoplay.stop();
			// 		mySwiper5.autoplay.stop();
			// 		startProgress();
			// 	} else {
			// 		$(this).addClass('isStopped');
			// 	}
			// });

			$('.tab_item').click(function () {
				var $sliderCont = $('.building_slider');
				$('.tab_item').removeClass('active');
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
				} else {
					$(this).addClass('active');
				}
				for (var i = 0; i < $sliderCont.length; i++) {
					var $blockElem = $($sliderCont[i]);
					if ($(this).data('id') === $blockElem.attr('id')) {
						$sliderCont.removeClass('active');
						if ($blockElem.hasClass('active')) {
							$blockElem.removeClass('active');
						} else {
							$blockElem.addClass('active');
						}
					}
				}
			});
		}
})
/******/]);
