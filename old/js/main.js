const pageId = $('main').attr('id');
function initPage() {
	let IS_ANIMATED_BURGER = false;
	$(window).scroll(function() {
		var scrollBottom = $(window).scrollTop() + $(window).height();
		var footer = $('footer').position().top;
		if ($(window).scrollTop() > 40) {
			$('header').addClass('active');
		} else {
			$('header').removeClass('active');
		}
		if (scrollBottom > footer) {
			$('footer').addClass('active');
		}
	});
	$(window).on('load', function () {
		$('header').addClass('show');
		if ($(window).scrollTop() > 40) {
			$('header').addClass('active');
		} else {
			$('header').removeClass('active');
		}
	});
	$('.menu_icon').click(function () {
		$('body').toggleClass('isOver pageLoad');
		if (IS_ANIMATED_BURGER) return;
		IS_ANIMATED_BURGER = true;

		if ($(this).parents('.header_container').hasClass('black')) {
			$('.header_container.desktop.white').addClass('blockHide');
			IS_ANIMATED_BURGER = false;
		}

		$('.menu_container').toggleClass('active');
		if ($(this).hasClass('isChecked')) {
			$('.menu_icon').removeClass('isAnimatedThird');
			setTimeout(function () {
				$('.menu_icon').removeClass('isAnimatedSecond');
			}, 300);
			setTimeout(function () {
				$('.menu_icon').removeClass('isAnimatedFirst');
			}, 600);
			setTimeout(function () {
				$('.menu_icon').removeClass('isChecked');
				IS_ANIMATED_BURGER = false;
			}, 900);
		} else {
			$('.menu_icon').addClass('isAnimatedFirst');
			setTimeout(function () {
				$('.menu_icon').addClass('isAnimatedSecond');
			}, 300);
			setTimeout(function () {
				$('.menu_icon').addClass('isAnimatedThird');
			}, 600);
			setTimeout(function () {
				$('.menu_icon').addClass('isChecked');
				IS_ANIMATED_BURGER = false;
			}, 900);
		}
		$('.header_container.desktop .menu_icon').addClass('isDisabled');
		setTimeout(function () {
			$('.header_container.desktop .menu_icon').removeClass('isDisabled');
		}, 800);
	});
	$('.show_more').click(function () {
		$(this).find('.my_btn').addClass('isHidden');
		setTimeout(function () {
			$('.show_more').css('display', 'none');
			$('.text').addClass('fullShow');
		}, 300);
	});
	const wow = new WOW(
		{
			boxClass:     'wow',
			animateClass: 'animated',
			offset:       0,
			mobile:       true,
			live:         true
		}
	);
	wow.init();
}

initPage();

switch (pageId) {
	case 'wrapper_main': {
		initMainPage();
		break;
	}
    case 'wrapper_contacts': {
        initContactsPage();
        break;
    }
	case 'wrapper_about': {
		initAboutPage();
		break;
	}
	case 'wrapper_portfolio': {
		initPortfolioPage();
		break;
	}
	case 'wrapper_selected_portfolio': {
		initSelectedPortfolioPage();
		break;
	}
	case 'wrapper_repair': {
		initRepairPage();
		break;
	}
	case 'wrapper_building': {
		initBuildingPage();
		break;
	}
}

function initMainPage() {
	const $leftBlock = $('.main_block.left');
	const $rightBlock = $('.main_block.right');

	$('.header_container.mobile.white').addClass('blockHide');
	$('.header_container.desktop.black').addClass('blockHide');

	$(window).on('load', function () {
		setTimeout(function () {
			$('header').addClass('show');
		});
		$('#wrapper_main').addClass('isLoaded');
		$('.main_block').addClass('active');
		$('.main_block .skew_line').addClass('active');
		setTimeout(function () {
			$('.main_block.right').css('pointer-events', 'visible');
		},1500);
	});

	$('.menu_icon').click(function () {
		if ($(this).parents('.header_container').hasClass('white')) {
			$(this).parents('.header_container').addClass('blockHide');
			$('.header_container.desktop.black').removeClass('blockHide');
		} else {
			setTimeout(function () {
				$('.header_container.desktop.black').addClass('blockHide');
				$('.header_container.desktop.white').removeClass('blockHide');
			}, 400);
		}
		$('.header_container.desktop .menu_icon').addClass('isDisabled');
		setTimeout(function () {
			$('.header_container.desktop .menu_icon').removeClass('isDisabled');
		}, 800);
	});

	$rightBlock.addClass('isRollUp');
	$leftBlock.hover(function () {
		$leftBlock.css('width', '85%');
		$rightBlock.css('width', '15%');
		$rightBlock.find('.skew_line').css({
			'right': '-40%',
			'width': '185%'
		});
		$rightBlock.addClass('isRollUp');
		$leftBlock.removeClass('isRollUp');
	});
	$rightBlock.hover(function () {
		$rightBlock.css('width', '65%');
		$leftBlock.css('width', '35%');
		$rightBlock.find('.skew_line').css({
			'right': '-10%',
			'width': '120%'
		});
		$leftBlock.addClass('isRollUp');
		$rightBlock.removeClass('isRollUp');
	});
}

function initContactsPage() {
	$('.header_container.mobile.white').addClass('blockHide');
	$('.header_container.desktop.white').addClass('blockHide');
	$(window).on('load', function () {
		$('.form-group, .custom_btn').addClass('active');
		$('.request_button').click(function () {
			for (let i = 0; i < $('.form-group').length; i++) {
				const $input = $($('.form-group')[i]).find('input');
				if ($('input.number').val().length < 13 && $input.val() === '' ) {
					$input.addClass('isError');
				} else {
					$input.removeClass('isError');
				}
			}
		});
	});
}

function initAboutPage() {
	$('.header_container.mobile.white').addClass('blockHide');
	$('.header_container.desktop.white').addClass('blockHide');

	var mySwiper = new Swiper('.about_slider', {
		slidesPerView: 1,
		spaceBetween: 30,
		loop: true,
		noSwiping: true,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '"></span>';
			},
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		// on: {
        //     slideChangeTransitionStart: function () {
        //         $('.swiper-slide-active').addClass('active');
        //         setTimeout(function () {
        //             $('.swiper-slide-active').removeClass('active');
        //         }, 800);
        //     }
		// }
	});

	$('.top_row').click(function () {
		$('.icon_cont').toggleClass('rotate');
		$(this).parent().find('.bottom_row').toggleClass('isHidden');
	});

	$(window).scroll(function() {
		var scrollBottom = $(window).scrollTop() + $(window).height();
		var slider = $('.about_slider').position().top;
		var footer = $('footer').position().top;
		console.log('Slider', slider);
		if (scrollBottom > slider) {
			$('.swiper-slide .image_container').addClass('active');
		}
		if (scrollBottom > footer) {
			$('footer').addClass('active');
		}
	});

	$('.swiper-button-next, .swiper-button-prev, .swiper-pagination-bullet').click(function () {
        $('.swiper-slide-active').addClass('active');
        setTimeout(function () {
            $('.swiper-slide-active').removeClass('active');
        }, 800);
	});

}

function initPortfolioPage() {
	$('header').addClass('black');
	$('.header_container.mobile.black').addClass('blockHide');
	$('.header_container.desktop.black').addClass('blockHide');

	$(window).on('load', function () {
		$('.section_top_info').addClass('active');
	});

	$(window).scroll(function() {
		if ($(window).scrollTop() > 40) {
			$('.header_container.white').addClass('blockHide');
			$('.header_container.black').removeClass('blockHide');
		} else {
			$('.header_container.white').removeClass('blockHide');
			$('.header_container.black').addClass('blockHide');
		}
	});

	$('.custom_btn').click(function () {
		$('.custom_btn').removeClass('isShow');
		if ($(this).hasClass('isShow')) {
			$(this).removeClass('isShow');
		} else {
			$(this).addClass('isShow');
		}
		for (let i = 0; i < $('.block_row').length; i++) {
			const $blockElem = $($('.block_row')[i]);
			if ($(this).data('id') === $blockElem.attr('id')) {
				$('.block_row').removeClass('isShow');
				if ($blockElem.hasClass('isShow')) {
					$blockElem.removeClass('isShow');
				} else {
					$blockElem.addClass('isShow');
				}
			}
		}
	});

	$('.menu_icon').click(function () {
		if ($(this).parents('.header_container.mobile').hasClass('white')) {
			$(this).parents('.header_container').addClass('blockHide');
			$('.header_container.mobile.black').removeClass('blockHide');
		} else {
			setTimeout(function () {
				$('.header_container.mobile.black').addClass('blockHide');
				$('.header_container.mobile.white').removeClass('blockHide');
			}, 400);
		}
		if ($(this).parents('.header_container.desktop').hasClass('white')) {
			$(this).parents('.header_container').addClass('blockHide');
			$('.header_container.desktop.black').removeClass('blockHide');
		} else if ($(this).parents('header').hasClass('active')) {
			setTimeout(function () {
				$('.header_container.desktop.black').removeClass('blockHide');
				$('.header_container.desktop.white').addClass('blockHide');
			}, 400);
		} else {
			setTimeout(function () {
				$('.header_container.desktop.black').addClass('blockHide');
				$('.header_container.desktop.white').removeClass('blockHide');
			}, 400);
		}
		$('.header_container.desktop .menu_icon').addClass('isDisabled');
		setTimeout(function () {
			$('.header_container.desktop .menu_icon').removeClass('isDisabled');
		}, 800);
	});
}

function initSelectedPortfolioPage() {

	function onElemScroll() {
		var scrollBottom = $(window).scrollTop() + $(window).height();
		var slider = $('.selected-portfolio-slider').position().top;
		var feedback = $('.feedback_block').position().top;
		var navBlock = $('.works_navigation').position().top;
		if (scrollBottom > slider) {
			$('.selected-portfolio-slider .image_container').addClass('active');
		}
		if (scrollBottom > feedback) {
			$('.feedback_block .image_container').addClass('active');
		}
		if (scrollBottom > navBlock) {
			$('.works_navigation .work_block').addClass('active');
		}
	}

	$('.header_container.mobile.white').addClass('blockHide');
	$('.header_container.desktop.white').addClass('blockHide');

	setTimeout(function () {
		$('.portfolio_info').addClass('active');
	}, 300);
	setTimeout(function () {
		$('.portfolio_info .image_container').addClass('active');
	}, 450);

	var mySwiper = new Swiper('.selected-portfolio-slider', {
		slidesPerView: 1,
		loop: true,
		speed: 400,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '"></span>';
			},
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});

	$(window).scroll(onElemScroll);

	$('.swiper-button-next, .swiper-button-prev, .swiper-pagination-bullet').click(function () {
		$('.swiper-slide-active').addClass('active');
		setTimeout(function () {
			$('.swiper-slide-active').removeClass('active');
		}, 1000);
	});
}

function initRepairPage() {
	let isChecked = false;
	function onElemScroll() {
		var scrollBottom = $(window).scrollTop() + $(window).height();
		const descImg = $('.description_section').position().top - 60;
		const gallery = $('.gallery_slider').position().top - 60;
		const feedback = $('.feedback_section').position().top - 60;
		const material = $('.material_info').position().top - 60;
		const objectCalc = $('.object_calc').position().top - 60;
		const review = $('.review_slider').position().top - 60;
		if (scrollBottom > descImg) {
			$('.description_section .image_container').addClass('active');
		}
		if (scrollBottom > gallery) {
			$('.gallery_slider .image_container').addClass('active');
			$('.gallery_slider .info_container').addClass('active');
		}
		if (scrollBottom > feedback) {
			$('.feedback_section').addClass('active');
		}
		if (scrollBottom > material) {
			$('.material_info').addClass('active');
			$('.material_info .image_container').addClass('active');
		}
		if (scrollBottom > objectCalc) {
			if(!isChecked) {
                $('.object_calc .top_info .image_container').addClass('active');
                setTimeout(function () {
                    $('.object_calc .top_info .image_container').removeClass('active');
                }, 800);
                isChecked = true;
			}
		}
		if (scrollBottom > review) {
			$('.review_slider .bottom_info').addClass('active');
			$('.review_slider .review_user_info').addClass('active');
			$('.review_slider').addClass('active');
		}
	}

	// var bar = new ProgressBar.Circle('#progress-circle', {
	// 	strokeWidth: 6,
	// 	easing: 'easeInOut',
	// 	duration: 3000,
	// 	color: '#FFEA82',
	// 	trailColor: '#eee',
	// 	trailWidth: 1,
	// 	svgStyle: null
	// });

	var mySwiper1 = new Swiper('.gallery_slider', {
		slidesPerView: 1,
		loop: true,
		spaceBetween: 15,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '"></span>';
			},
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
	var mySwiper2 = new Swiper('.review_slider', {
		slidesPerView: 1,
		loop: true,
		speed: 1000,
		effect: 'fade',
		autoplay: {
			delay: 5000,
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '">' +  (index + 1) + '</span>';
			},
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		}
	});

	$(window).scroll(function() {
		if ($(window).scrollTop() > 40) {
			$('.header_container.half').addClass('onBlack');
		} else {
			$('.header_container.half').removeClass('onBlack');
		}
		onElemScroll();
	});

	$('.menu_icon').click(function () {
        if (!$('.header_container.half').hasClass('onBlack')) {
            $('.header_container.half').addClass('onBlack');
        } else {
            $('.header_container.half').removeClass('onBlack');
		}
	});

	$('.swiper-button-next, .swiper-button-prev, .swiper-pagination-bullet').click(function () {
		$('.swiper-slide-active').addClass('active');
		setTimeout(function () {
			$('.swiper-slide-active').removeClass('active');
		}, 800);
	});
}

function initBuildingPage() {
    let isChecked = false;
    let isCheckedWorks = false;
    function onElemScroll() {
        var scrollBottom = $(window).scrollTop() + $(window).height();
        const works = $('.building_slider').position().top - 60;
        const gallery = $('.gallery_slider').position().top - 60;
        const objectCalc = $('.object_calc').position().top - 60;
        const review = $('.review_slider').position().top - 60;
        if (scrollBottom > works) {
            if (!isCheckedWorks) {
                $('.building_slider .image_container').addClass('active');
                setTimeout(function () {
                    $('.building_slider .image_container').removeClass('active');
                }, 800);
                isCheckedWorks = true;
			}
		}
        if (scrollBottom > gallery) {
            $('.gallery_slider .image_container').addClass('active');
            $('.gallery_slider .info_container').addClass('active');
        }
        if (scrollBottom > objectCalc) {
            if (!isChecked) {
                $('.object_calc .top_info .image_container').addClass('active');
                setTimeout(function () {
                    $('.object_calc .top_info .image_container').removeClass('active');
                }, 800);
                isChecked = true;
            }
        }
        if (scrollBottom > review) {
            $('.review_slider .bottom_info').addClass('active');
            $('.review_slider .review_user_info').addClass('active');
            $('.review_slider').addClass('active');
        }
    }

    $(window).scroll(function() {
        onElemScroll();
    });

	$('.header_container.mobile.white').addClass('blockHide');
	$('.header_container.desktop.white').addClass('blockHide');

	var mySwiper1 = new Swiper('.gallery_slider', {
		slidesPerView: 1,
		loop: true,
		spaceBetween: 15,
		speed: 400,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '"></span>';
			},
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
	var mySwiper2 = new Swiper('.review_slider', {
		slidesPerView: 1,
		loop: true,
		speed: 1000,
		effect: 'fade',
		autoplay: {
			delay: 5000,
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '">' +  (index + 1) + '</span>';
			},
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		}
	});
	var mySwiper3 = new Swiper('#foundation', {
		slidesPerView: 1,
		loop: true,
		speed: 1000,
		effect: 'fade',
		autoplay: {
			delay: 5000,
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '">' +  (index + 1) + '</span>';
			},
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		}
	});
	var mySwiper4 = new Swiper('#houses', {
		slidesPerView: 1,
		loop: true,
		speed: 1000,
		effect: 'fade',
		autoplay: {
			delay: 5000,
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '">' +  (index + 1) + '</span>';
			},
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		}
	});
	var mySwiper5 = new Swiper('#landscape', {
		slidesPerView: 1,
		loop: true,
		speed: 1000,
		effect: 'fade',
		autoplay: {
			delay: 5000,
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '">' +  (index + 1) + '</span>';
			},
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		}
	});

	$('.tab_item').click(function () {
		const $sliderCont = $('.building_slider');
		$('.tab_item').removeClass('active');
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			$(this).addClass('active');
		}
		for (let i = 0; i < $sliderCont.length; i++) {
			const $blockElem = $($sliderCont[i]);
			if ($(this).data('id') === $blockElem.attr('id')) {
				$sliderCont.removeClass('active');
				if ($blockElem.hasClass('active')) {
					$blockElem.removeClass('active');
				} else {
					$blockElem.addClass('active');
				}
			}
		}
	});

    $('.swiper-button-next, .swiper-button-prev, .swiper-pagination-bullet').click(function () {
        $(this).parents('.swiper-container-horizontal').find('.swiper-slide-active').addClass('active');
        setTimeout(function () {
            $('.swiper-slide-active').removeClass('active');
        }, 800);
    });
}