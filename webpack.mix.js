const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.babel('public/js/main.js');
// mix.babel('public/js/admin/main.js');

mix.js('public/js/main.js', 'public/js/main.js');
mix.js('public/js/admin/main.js', 'public/js/admin/main.js');

// mix.minify('public/js/main.js');
// mix.minify('public/js/admin/main.js');

mix.minify('public/css/main.css');
mix.minify('public/css/animate.css');
mix.minify('public/css/admin/main.css');
