@extends('layouts.app')
@section('title', 'О компании')
@section('content')
    <main id="wrapper_about">
        <section class="about_info">
        <div itemscope="" itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" style="margin-top:0;margin-bottom:30px;">
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a rel="nofollow" itemprop="item" title="Строительная компания в Одессе" href="{{ route('index') }}">
          <span itemprop="name">Строительная компания в Одессе</span>
          <meta itemprop="position" content="1">
       </a>
   </span> > 
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a itemprop="item" title="О компании" href="{{ route('about') }}">
          <span itemprop="name">О компании</span>
          <meta itemprop="position" content="2">
       </a>
   </span>
</div>
            <div class="container">
            
                <div class="about_block row m-0">
                    <div class="col-xl-5 col-md-6 col-12 p-0 wow animated fadeInRight" data-wow-duration="0.8s" data-wow-delay="0.2s">
                        <h1 class="title first">О компании</h1>
                        <p class="text first">
                        Основное направление компании Attic Stroy - это ремонт квартир и строительство частных домов. После 10 лет работы в различных строительных компаниях Одессы мы получили достаточно опыта, чтобы понять, какой уровень услуг и какой сервис нужен рынку Одессы.  Главные проблемы одесского рынка строительных и отделочных услуг - это завышенные либо неправдоподобные сметы, невыполнение работы в сроки, отсутствие каких-либо гарантий и низкий клиентский сервис.
                        </p>
                    </div>
                    <div class="col-xl-7 col-md-6 col-12 p-0 wow animated fadeInLeft" data-wow-duration="0.8s">
                        <div class="image_cont">
                            <img src="{{ asset('images/about/about-image-1.jpg') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="about_block row m-0">
                    <div class="col-xl-7 col-md-6 d-none d-md-block p-0 wow animated fadeInRight" data-wow-duration="0.8s" data-wow-delay="0.2s">
                        <div class="image_cont">
                            <img src="{{ asset('images/about/about-image-2.jpg') }}" alt="">
                        </div>
                    </div>
                    <div class="col-xl-5 col-md-6 col-12 p-0 wow animated fadeInLeft" data-wow-duration="0.8s">
                        <h1 class="title to-left">Наша миссия</h1>
                        <p class="text to-left">
                        Мы хотим чтобы процесс строительства или ремонта, а по сути, создания своего домашнего очага, был для человека вдохновляющим, интересным и легким. Мы работаем с самыми разнообразными и креативными проектами, помогая клиенту создать свою домашнюю атмосферу в рамках его потребностей.</p>
                    </div>
                    <div class="col-sm-6 col-12 d-block d-md-none p-0 wow animated fadeInRight" data-wow-duration="0.8s" data-wow-delay="0.2s">
                        <div class="image_cont">
                            <img src="{{ asset('images/about/about-image-2.jpg') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="about_block row m-0">
                    <div class="col-xl-5 col-md-6 col-12 p-0 wow animated fadeInRight" data-wow-duration="0.8s" data-wow-delay="0.2s">
                        <h1 class="title to-left last">Наша цель</h1>
                        <p class="text to-left last">
                        Attic Stroy - это не рядовая строительная компания, которая просто выполняет свои обязательства по договору. Мы хотим чтобы каждый человек в Одессе, а потом и в Украине, после покупки своей квартиры или участка земли, мог легко и уверенно создать с нашей помощью настоящий дом.  Для кого-то это будет первая малогабаритная квартира-студия на 30 квадратных метров, а для кого-то частный  дом своей мечты. И мы знаем, что дом - это не просто 4 стены.</p>
                    </div>
                    <div class="col-xl-7 col-md-6 col-12 p-0 wow animated fadeInLeft" data-wow-duration="0.8s">
                        <div class="image_cont">
                            <img src="{{ asset('images/about/about-image-3.jpg') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="stages">
            <div class="container">
                <div class="section_title wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0s">
                    <h1 class="title">Этапы работы</h1>
                </div>
                <div class="row m-0">
                    <div class="offset-md-1 col-md-4 col-6 p-0 wow animated fadeInRight" data-wow-duration="0.7s" data-wow-delay="0.15s">
                        <div class="stage_block">
                            <div class="stage_img">
                                <!-- <img src="{{ asset('images/about/stage-img.png') }}" alt=""> -->
                                <p>1</p>
                            </div>
                            <div class="stage_text">
                                <p class="text">Подготовка</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 d-none d-md-block p-0 wow animated fadeInRight" data-wow-duration="0.7s" data-wow-delay="0.3s">
                        <div class="arrow_img_cont">
                            <img src="{{ asset('images/about/arrow-about-img.svg') }}" alt="">
                        </div>
                    </div>
                    <div class="offset-md-1 col-md-4 col-6 p-0 wow animated fadeInRight" data-wow-duration="0.7s" data-wow-delay="0.45s">
                        <div class="stage_block">
                            <div class="stage_img">
                                <!-- <img src="{{ asset('images/about/stage-img.png') }}" alt=""> -->
                                <p>2</p>
                            </div>
                            <div class="stage_text">
                                <p class="text">Расчёт стоимости</p>
                            </div>
                        </div>
                    </div>
                    <div class="offset-md-1 col-md-4 col-6 p-0 wow animated fadeInRight" data-wow-duration="0.7s" data-wow-delay="0.6s">
                        <div class="stage_block">
                            <div class="stage_img">
                                <!-- <img src="{{ asset('images/about/stage-img.png') }}" alt=""> -->
                                <p>3</p>
                            </div>
                            <div class="stage_text">
                                <p class="text">Начало работ</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 d-none d-md-block p-0 wow animated fadeInRight" data-wow-duration="0.7s" data-wow-delay="0.75s">
                        <div class="arrow_img_cont">
                            <img src="{{ asset('images/about/arrow-about-img.svg') }}" alt="">
                        </div>
                    </div>
                    <div class="offset-md-1 col-md-4 col-6 p-0 wow animated fadeInRight" data-wow-duration="0.7s" data-wow-delay="0.9s">
                        <div class="stage_block">
                            <div class="stage_img">
                                <!-- <img src="{{ asset('images/about/stage-img.png') }}" alt=""> -->
                                <p>4</p>
                            </div>
                            <div class="stage_text">
                                <p class="text">Черновые и инженерные работы</p>
                            </div>
                        </div>
                    </div>
                    <div class="offset-md-1 col-md-4 col-6 p-0 wow animated fadeInRight" data-wow-duration="0.7s" data-wow-delay="1.05s">
                        <div class="stage_block">
                            <div class="stage_img">
                                <!-- <img src="{{ asset('images/about/stage-img.png') }}" alt=""> -->
                                <p>5</p>
                            </div>
                            <div class="stage_text">
                                <p class="text">Чистовая отделка</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 d-none d-md-block p-0 wow animated fadeInRight" data-wow-duration="0.7s" data-wow-delay="1.2s">
                        <div class="arrow_img_cont">
                            <img src="{{ asset('images/about/arrow-about-img.svg') }}" alt="">
                        </div>
                    </div>
                    <div class="offset-md-1 col-md-4 col-6 p-0 wow animated fadeInRight" data-wow-duration="0.7s" data-wow-delay="1.35s">
                        <div class="stage_block">
                            <div class="stage_img">
                                <!-- <img src="{{ asset('images/about/stage-img.png') }}" alt=""> -->
                                <p>6</p>
                            </div>
                            <div class="stage_text">
                                <p class="text">Финальные штрихи</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="custom_btn wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="1.5s">
                    <a href="#" class="my_btn" data-toggle="modal" data-target="#stages_modal"><span>Подробнее</span></a>
                </div> -->
            </div>
        </section>
        <section class="slider">
            <div class="container">
                <div class="section_title wow animated fadeInUp" data-wow-duration="0.6s">
                    <h1 class="title">Квартира будет выглядеть так, как вы захотите. На выбор — сотни комбинаций цветов
                        и материалов</h1>
                </div>
                <!-- Slider main container -->
                <div class="swiper-container about_slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide swiper-no-swiping">
                            <div class="image_container">
                                <img src="{{ asset('images/about/slide-image.png') }}" alt="">
                            </div>
                            <div class="text_bottom_slide">
                                <p class="text">Выразительный французский стиль: серые стены, тёмный паркет,
                                    классические двери, высокий белый плинтус.</p>
                            </div>
                        </div>
                        <div class="swiper-slide swiper-no-swiping">
                            <div class="image_container">
                                <img src="{{ asset('images/about/slide-image.png') }}" alt="">
                            </div>
                            <div class="text_bottom_slide">
                                <p class="text">Выразительный французский стиль: серые стены, тёмный паркет,
                                    классические двери, высокий белый плинтус.</p>
                            </div>
                        </div>
                        <div class="swiper-slide swiper-no-swiping">
                            <div class="image_container">
                                <img src="{{ asset('images/about/slide-image.png') }}" alt="">
                            </div>
                            <div class="text_bottom_slide">
                                <p class="text">Выразительный французский стиль: серые стены, тёмный паркет,
                                    классические двери, высокий белый плинтус.</p>
                            </div>
                        </div>
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.55s"></div>
                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev wow animated fadeInLeft" data-wow-duration="0.6s" data-wow-delay="0.55s">
                        <img src="{{ asset('images/about/slide-arrow.svg') }}" alt="">
                    </div>
                    <div class="swiper-button-next wow animated fadeInRight" data-wow-duration="0.6s" data-wow-delay="0.55s">
                        <img src="{{ asset('images/about/slide-arrow.svg') }}" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section class="guarantee">
            <div class="container">
                <div class="section_title wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0s">
                    <h1 class="title">Вопрос-ответ</h1>
                </div>
                <div class="question_block wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.15s">
                    <div class="top_row">
                        <p class="text">Чем компания ATTIC-STROY отличается от остальных?</p>
                        <div class="icon_cont">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="bottom_row isHidden bigBox">
                        <p class="text">
                        Наши специалисты выполняют профессиональный ремонт и строительство любой сложности : квартир, коммерческой недвижимости, частных домов, коттеджей и гостиниц. Мы внедряем современные технологии во все процессы, меняя тем самым подход к выполнению ремонта, делая его для наших клиентов простым и максимально прозрачным.</p>
                    </div>
                </div>
                <div class="question_block wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.3s">
                    <div class="top_row">
                        <p class="text">Что входит в стоимость ремонта?</p>
                        <div class="icon_cont">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="bottom_row isHidden">
                        <p class="text">В стоимость входит капитальный ремонт квартиры: все черновые, чистовые материалы и все виды работ, контроль качества и прописанная в договоре гарантия на 5 лет. Цена фиксируется после подписания договора и не меняется на протяжении всего ремонта.</p>
                    </div>
                </div>
                <div class="question_block wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.45s">
                    <div class="top_row">
                        <p class="text">Сколько времени занимает ремонт?</p>
                        <div class="icon_cont">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="bottom_row isHidden">
                        <p class="text">Ремонт квартир длится от 2 до 6 месяцев в зависимости от объекта, сложности и масштаба работ. Сроки ремонта оговариваются и фиксируются при подписании договора. Наши специалисты завершают работы точно в срок.</p>
                    </div>
                </div>
                <div class="question_block wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.6s">
                    <div class="top_row">
                        <p class="text">Можно ли сделать ремонт только в одном помещении — например, в ванной или на кухне?</p>
                        <div class="icon_cont">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="bottom_row isHidden smallBox">
                        <p class="text">Нет. Мы делаем только комплексный ремонт всей квартиры.</p>
                    </div>
                </div>
                <div class="question_block wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.75s">
                    <div class="top_row">
                        <p class="text">Можно ли заказать строительство частного дома или таунхауса?</p>
                        <div class="icon_cont">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="bottom_row isHidden smallBox">
                        <p class="text">Да, конечно. На данный момент мы выполнили несколько успешных проектов и готовы ознакомить Вас с результатами нашей работы.</p>
                    </div>
                </div>
                <div class="question_block wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.75s">
                    <div class="top_row">
                        <p class="text">Можно ли использовать свои материалы?</p>
                        <div class="icon_cont">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="bottom_row isHidden">
                        <p class="text">Мы не против, что бы Вы закупали материалы, однако даём рекомендацию относительно производителя, так как мы не работаем с материалами низкого качества. Только так мы можем гарантировать качество ремонта.</p>
                    </div>
                </div>
                <div class="question_block wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.75s">
                    <div class="top_row">
                        <p class="text">Можете ли вы подобрать и заказать мебель?</p>
                        <div class="icon_cont">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="bottom_row isHidden smallBox">
                        <p class="text">Да, наша компания занимается полным спектром услуг касаемо ремонта, в том числе заказом кухни, шкафов, кроватей и всей бытовой техники.</p>
                    </div>
                </div>
                <div class="section_title wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.9s">
                    <h1 class="title">Стоимость и оплата</h1>
                </div>
                <div class="question_block wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="1.05s">
                    <div class="top_row">
                        <p class="text">Как происходит оплата?</p>
                        <div class="icon_cont">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="bottom_row isHidden">
                        <p class="text">
                        Мы работаем как с полной, так и с частичной оплатой, разбивая все работы на простые и понятные этапы.
                        </p>
                    </div>
                </div>
                <div class="question_block wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="1.2s">
                    <div class="top_row">
                        <p class="text">Сколько стоит выезд замерщика?</p>
                        <div class="icon_cont">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="bottom_row isHidden smallBox">
                        <p class="text">Выезд замерщика — бесплатный.</p>
                    </div>
                </div>  
            </div>
        </section>
        <!--<section class="about_bottom">
            <div class="container">
                <div class="section_title wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0s">
                    <h1 class="title">Что я такое?</h1>
                </div>
                <div class="text_block wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.15s">
                    <p class="text">С другой стороны сложившаяся структура организации позволяет выполнять важные
                        задания по разработке новых предложений. Задача организации, в особенности же дальнейшее
                        развитие различных форм деятельности позволяет оценить значение форм развития. Повседневная
                        практика показывает, что начало повседневной работы по формированию позиции представляет собой
                        интересный эксперимент проверки дальнейших направлений развития. Задача организации, в
                        особенности же постоянное информационно-пропагандистское обеспечение нашей деятельности требуют
                        определения и уточнения позиций, занимаемых участниками в отношении поставленных задач.<br>
                        <br>
                        Разнообразный и богатый опыт сложившаяся структура организации влечет за собой процесс внедрения
                        и модернизации систем массового участия. Таким образом реализация намеченных плановых заданий
                        обеспечивает широкому кругу (специалистов) участие в формировании модели развития. Задача
                        организации, в особенности же реализация намеченных плановых заданий обеспечивает широкому кругу
                        (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных
                        задач. С другой стороны укрепление и развитие структуры в значительной степени обуславливает
                        создание дальнейших направлений развития. С другой стороны новая модель организационной
                        деятельности позволяет оценить значение модели развития.
                    </p>
                </div>
                <div class="custom_btn show_more wow animated fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.3s">
                    <div class="my_btn"><span>Показать полностью</span></div>
                </div>
            </div>
        </section>-->
    </main>
@endsection


