@extends('layouts.app')
@section('title', 'Ремонт квартир Одесса - цена, стоимость услуг, отзывы | Attic-stroy')
@section('desc', 'КомпанияATTIC-STROYпредлагает ремонт квартир в Одессе дизайн интерьеров Звоните  +38 (063) 153-53-33')
@section('content')
    <main id="wrapper_repair">
        <!--<section class="main_page">
            <canvas class="d-none d-lg-block" id="canvasRegn"></canvas>
            <div class="row m-0">
            <div itemscope="" itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs">
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a rel="nofollow" itemprop="item" title="Строительная компания в Одессе" href="{{ route('index') }}">
          <span itemprop="name">Строительная компания в Одессе</span>
          <meta itemprop="position" content="1">
       </a>
   </span> > 
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a itemprop="item" title=" Ремонт квартир" href="{{ route('repair') }}">
          <span itemprop="name">Ремонт квартир</span>
          <meta itemprop="position" content="2">
       </a>
   </span>
</div>
                <div class="col-xl-6 col-md-5 col-12 text_block p-0">
                    <div class="description_block wow animated fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.05s">
                        <h1 class="title">Акция - ремонт квартир под ключ от 200 y.е. за 1 м2</h1>
                        <h1 class="title">Квартиру своей мечты нельзя просто купить, ее можно только создать самому.</h1><p class="text"> Наша команда реализует дизайнерские проекты любой сложности. Прозрачные условия работы, точные сметы и гарантия на все выполненные услуги.</p>
                        <h4 class="subtitle mt-3">Комплексный ремонт от 200 у.е.</h4>
                    </div>
                </div>
                <div class="col-xl-6 col-md-7 col-12 sofa_block p-0">
                    <div class="image_container wow animated fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.15s">
                        <img src="{{ asset('images/repair/sopha-new.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </section>-->
        <section class="build_info_section">
            <div class="container">
            <div itemscope="" itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" style="margin-top:0;margin-bottom:30px">
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a rel="nofollow" itemprop="item" title="Строительная компания в Одессе" href="{{ route('index') }}">
          <span itemprop="name">Строительная компания в Одессе</span>
          <meta itemprop="position" content="1">
       </a>
   </span> > 
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a itemprop="item" title=" Ремонт квартир" href="{{ route('repair') }}">
          <span itemprop="name">Ремонт квартир</span>
          <meta itemprop="position" content="2">
       </a>
   </span>
</div>
                <div class="row m-0 top_row">
                
                    <div class="col-12 p-0 d-block d-md-none wow animated fadeInLeft" data-wow-duration="0.8s"
                         data-wow-delay="0s">
                        <div class="block_info top">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/sopha-new.jpg') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 p-0 d-block d-md-none wow animated fadeInLeft" data-wow-duration="0.8s"
                         data-wow-delay="0.2s">
                        <div class="block_info top">
                            <div class="text_info">
                            <h1 class="title">Квартиру своей мечты нельзя просто купить, ее можно только создать самому.</h1>
                                <p class="text"> Наша команда реализует дизайнерские проекты любой сложности. Прозрачные условия работы, точные сметы и гарантия на все выполненные услуги.</p>
                        <h4 class="subtitle mt-3">Комплексный ремонт от 200 у.е.</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-5 p-0 d-none d-md-block wow animated fadeInRight" data-wow-duration="0.8s"
                         data-wow-delay="0s">
                        <div class="block_info top">
                            <div class="text_info">
                            <h1 class="title">Квартиру своей мечты нельзя просто купить, ее можно только создать самому.</h1>
                                <p class="text"> Наша команда реализует дизайнерские проекты любой сложности. Прозрачные условия работы, точные сметы и гарантия на все выполненные услуги.</p>
                        <h4 class="subtitle mt-3">Комплексный ремонт от 200 у.е.</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-7 p-0 d-none d-md-block wow animated fadeInLeft" data-wow-duration="0.8s"
                         data-wow-delay="0.2s">
                        <div class="block_info top">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/sopha-new.jpg') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="row m-0 bottom_row">
                    <div class="col-xl-6 col-md-7 col-12 p-0 wow animated fadeInRight" data-wow-duration="0.8s"
                         data-wow-delay="0s">
                        <div class="block_info bottom">
                            <div class="image_container">
                                <img src="{{ asset('images/building/second-image.jpg') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-5 col-12 p-0 wow animated fadeInLeft" data-wow-duration="0.8s"
                         data-wow-delay="0.2s">
                        <div class="block_info bottom">
                            <div class="text_info">
                                <h2 class="title">Технологии строительства</h2>
                                <p class="text">
                                В работе мы используем только современные и проверенные десятилетиями технологии строительства. Мы делаем все, чтобы гарантировать долгосрочную эксплуатацию вашего загородного дома. Для каждого проекта наши специалисты индивидуально подбирают материалы и технические решения. Мы всегда учитываем климатические и геологические факторы, выбирая технологии с повышенным запасом прочности. Поэтому на все наши дома гарантия по договору от 10 лет.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>-->
                <!--<div class="title_cont">
                    <h2 class="title">Стоимость строительства от 325 у.е.</h2>
                </div>-->
            </div>
        </section>
        <section class="section_price">
            <div class="container">
                <div class="row m-0 price_blocks">
                    <div class="section_title wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <h2 class="title">Акция</h2>
                    </div>
                    <div class="col-12 col-md-6 wow fadeInUp mb-4 mb-md-0" data-wow-duration="1s" data-wow-delay="0.15s">
                        <div class="block_container">
                            <h4 class="title">Пакет «Стандарт»</h4>
                            <p class="price">215 у.е./м2</p>
                            <p class="description">Электрика</p>
                            <p class="description">Стены</p>
                            <p class="description">Сантехника</p>
                            <p class="description">Потолки</p>
                            <p class="description">Полы</p>
                            <p class="description">Подготовка к сдаче квартиры</p>
                            <s class="description">Климатическая установка</s>
                            <s class="description">Шторы</s>
                            <s class="description">Кухня под заказ</s>
                            <s class="description">Спальный гарнитур</s>
                            <div class="custom_btn">
                                <div class="my_btn" data-toggle="modal" data-target="#modal_standart">
                                    <span>Подробнее</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="block_container black">
                            <h4 class="title">Пакет «Комфорт»</h4>
                            <p class="price">285 у.е./м2</p>
                            <p class="description">Электрика</p>
                            <p class="description">Стены</p>
                            <p class="description">Сантехника</p>
                            <p class="description">Потолки</p>
                            <p class="description">Полы</p>
                            <p class="description">Подготовка к сдаче квартиры</p>
                            <p class="description">Климатическая установка</p>
                            <p class="description">Шторы</p>
                            <p class="description">Кухня под заказ</p>
                            <p class="description">Спальный гарнитур</p>
                            <div class="custom_btn">
                                <div class="my_btn" data-toggle="modal" data-target="#modal_comfort">
                                    <span>Подробнее</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="feedback_section">
            <div class="title_cont wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.15s">
                <h2 class="title">Получите бесплатную консультацию по выбору строительных материалов и сэкономьте до 20 процентов на ремонте</h2>
            </div>
            <div class="custom_btn wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="my_btn" data-toggle="modal" data-target="#feedback_modal"><span>Связаться с нами</span></div>
            </div>
        </section>
        <section class="service_section">
            <div class="container">
                <div class="section_title">
                    <h3 class="title wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">Услуги</h3>
                </div>
                <div class="row m-0">
                    <div class="col-lg-4 col-md-6 col-12 service_block wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.15s">
                        <div class="image_cont">
                            <img src="{{ asset('images/repair/kuhn.jpg') }}" alt="">
                        </div>
                        <p class="text title">Индивидуальная мебель</p>
                        <p class="text desc">Индивидуальная мебель под заказ для вашей квартиры. Никаких компромиссов между размерами, стилем и качеством. Идеальное решение в каждом конкретном случае. </p>
                        <div class="custom_btn">
                            <div class="my_btn" data-toggle="modal" data-target="#service_furniture">
                                <span>Подробнее</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 service_block wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="image_cont">
                            <img src="{{ asset('images/repair/shumizol.jpg') }}" alt="">
                        </div>
                        <p class="text title">Шумоизоляция</p>
                        <p class="text desc">Ваша квартира это не просто отделка и мебель, это еще и атмосфера. Мы сделаем так, чтобы вашу атмосферу не нарушали посторонние звуки с улицы или от соседей.</p>
                        <div class="custom_btn">
                            <div class="my_btn" data-toggle="modal" data-target="#service_insulation">
                                <span>Подробнее</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 service_block wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.45s">
                        <div class="image_cont">
                            <img src="{{ asset('images/repair/shpack.jpg') }}" alt="">
                        </div>
                        <p class="text title">Шпатлевка</p>
                        <p class="text desc">Высокая скорость нанесения материала и более рациональное использование смесей делает механизированную шпаклевку незаменимым средством в отделочных работах.</p>
                        <div class="custom_btn">
                            <div class="my_btn" data-toggle="modal" data-target="#service_putty">
                                <span>Подробнее</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="description_section">
            <div class="row m-0">
                <div class="col-12 p-0 d-block d-lg-none wow animated fadeInRight" data-wow-duration="0.6s" data-wow-delay="0s">
                    <div class="text_info">
                        <h2 class="title">Гарантия качества по договору</h2>
                        <p class="text">
                        На все выполненные нами работы предоставляется гарантия на 5 лет. Мы заботимся о своей репутации и подтверждаем это в каждом подписанном договоре. Мы уверены в качестве закупаемых материалов, в опыте и квалификации каждого сотрудника, поэтому можем дать точную гарантию.
                        </p>
                    </div>
                </div>
                <div class="col-lg-7 col-12 p-0">
                    <div class="image_container">
                        <img src="{{ asset('images/repair/repair-img-1.jpg') }}" alt="">
                    </div>
                </div>
                <div class="col-lg-5 p-0 d-none d-lg-block wow animated fadeInLeft" data-wow-duration="0.6s" data-wow-delay="0.35s">
                    <div class="text_info">
                        <h2 class="title">Гарантия качества по договору</h2>
                        <p class="text">
                        На все выполненные нами работы предоставляется гарантия на 5 лет. Мы заботимся о своей репутации и подтверждаем это в каждом подписанном договоре. Мы уверены в качестве закупаемых материалов, в опыте и квалификации каждого сотрудника, поэтому можем дать точную гарантию.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row m-0">
                <div class="left_block_desktop col-lg-5 col-12 p-0 wow animated fadeInRight" data-wow-duration="0.6s" data-wow-delay="0.7s">
                    <div class="text_info">
                        <h2 class="title">Прозрачные условия работы</h2>
                        <p class="text">
                        Перед началом работ мы утверждаем проектные сметы и детально раскрываем все будущие расходы. Мы заранее предупреждаем обо всех подводных камнях и нюансах, которые сопровождают любой дизайнерский ремонт.
                        </p>
                    </div>
                </div>
                <div class="right_block_desktop col-lg-7 col-12 p-0">
                    <div class="image_container">
                        <img src="{{ asset('images/repair/repair-img-2.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="slide_section">
            <div class="container">
                <div class="title_cont wow animated fadeInUp" data-wow-duration="0.6s">
                    <h3 class="title">Виды работ</h3>
                </div>
                <div class="row m-0 work_types">
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_1.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Дизайн проект</h4>
                                <p class="text">Чтобы Ваши ожидания были оправданы и не расходились с конечным предлагаем создать дизайн-проект будущей квартиры.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.15s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_2.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Перепланировка</h4>
                                <p class="text">Демонтируем старые и возведем новые стены.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_3.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Теплый пол</h4>
                                <p class="text">Выполним монтаж электрической либо водяной системы теплых полов.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.45s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_4.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Стяжка</h4>
                                <p class="text">Выравниваем поверхность пола перед декоративным покрытием.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_5.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Электрика</h4>
                                <p class="text">Произведём монтаж электрики любой сложности.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.75s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_6.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Штукатурка</h4>
                                <p class="text">Используем штукатурку машинного нанесения по немецкой технологии.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_7.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Сантехника</h4>
                                <p class="text">Качественно выполним монтаж отопления, водоснабжения и водоотведения.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.05s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_8.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Потолки</h4>
                                <p class="text">Монтаж натяжных либо гипсокартонных потолков.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_9.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Малярные работы</h4>
                                <p class="text">Любые виды ручной, а так же механизированной отделки готовых поверхностей.</p>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.35s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_10.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Плитка</h4>
                                <p class="text">Все виды плиточных работ.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_11.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Полы</h4>
                                <p class="text">Произведём монтаж ламината, паркетной доски либо паркета.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.65s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_12.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Окна и двери</h4>
                                <p class="text">Замеры, изготовление и монтаж окон и дверей на любой вкус.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.8s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_13.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Шумоизоляция</h4>
                                <p class="text">Создадим тишину и комфорт в Вашем доме.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.95s">
                        <div class="type_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/works-icons/attic-icons_14.svg') }}" alt="">
                            </div>
                            <div class="type_info">
                                <h4 class="type_title">Мебель</h4>
                                <p class="text">Изготовим любую мебель на Ваш вкус и бюджет.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="material_info">
            <div class="row m-0">
                <div class="col-sm-8 col-12 p-0">
                    <div class="info_block wow animated fadeInDown" data-wow-duration="0.6s" data-wow-delay="0.1s">
                        <h2 class="title">Помощь в закупке материалов</h2>
                        <p class="text">
                            Мы не просто консультируем в выборе материалов исходя из ваших запросов, но и помогаем с покупкой и доставкой. Мы также готовы предложить дополнительную скидку при покупке различных строительных материалов у наших партнеров. И это не просто уверенность в качестве продукции, но и реальная экономия.</p>
                    </div>
                </div>
                <div class="col-sm-4 col-12 p-0">
                    <div class="image_container">
                        <img src="{{ asset('images/repair/repair-img-3.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section class="review_slide_section">
            <div class="review_slider">
                <div class="swiper-wrapper">


                    @if ($commentsList)
                    @foreach ($commentsList as $comment)
                    <div class="swiper-slide">
                        <div class="row m-0">
                            <div class="col-12 p-0 d-block d-md-none">
                                <div class="review_user_info">

                                    @if(\App\Http\Helpers\Helpers::isStorageFileExists($comment->image))
                                        <div class="img_cont">
                                            <img src="{{ \App\Http\Helpers\Helpers::imagePath($comment->image) }}" alt="">
                                        </div>
                                    @endif
                                    <h1 class="title">{{$comment->name}}</h1>
                                    <h5 class="sub_title">{{$comment->position}}</h5>
                                    <div class="text-center mt-2"><a href="{{ $comment->link }}">{{ $comment->link }}</a></div>

                                </div>
                            </div>
                            <div class="col-lg-8 col-md-9 col-12 p-0">
                                <div class="bottom_info">
                                    <p class="text">
                                        {{$comment->comment}}
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-3 p-0 d-none d-md-block">
                                <div class="review_user_info">

                                    @if(\App\Http\Helpers\Helpers::isStorageFileExists($comment->image))
                                        <div class="img_cont">
                                            <img src="{{ \App\Http\Helpers\Helpers::imagePath($comment->image) }}" alt="">
                                        </div>
                                    @endif
                                    <h1 class="title">   {{$comment->name}}  </h1>
                                    <h5 class="sub_title">   {{$comment->position}}  </h5>
                                        <div class="text-center mt-2"><a href="{{ $comment->link }}" >{{ $comment->link }}</a></div>

                                </div>
                            </div>
                        </div>
                    </div>
                  @endforeach
                    @endif



                </div>
                <div class="swiper-pagination">
                </div>
                <div class="swiper-button-prev">
                    <i class="arrow left"></i>
                </div>
                <div class="swiper-button-next">
                    <i class="arrow right"></i>
                </div>
                <div class="slide-circle"></div>
            </div>
        </section>
        <section class="advantages_section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-12 wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0s">
                        <div class="advantage_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/attic-icons_time.svg') }}" alt="">
                            </div>
                            <div class="text_info">
                                <h2 class="title">Скорость</h2>
                                <p class="text">Выполнение работ точно в срок по договору или раньше.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-12 wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.15s">
                        <div class="advantage_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/attic-icons_quality.svg') }}" alt="">
                            </div>
                            <div class="text_info">
                                <h2 class="title">Качество</h2>
                                <p class="text">Гарантия на все выполненные работы, прописанная в договоре.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-12 wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">
                        <div class="advantage_block">
                            <div class="image_container">
                                <img src="{{ asset('images/repair/attic-icons_warranty.svg') }}" alt="">
                            </div>
                            <div class="text_info">
                                <h2 class="title">Честность</h2>
                                <p class="text">Прозрачные и понятные финансовые сметы, детальная отчетность на каждом этапе.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="object_calc">
            <div class="container">
                <div class="top_info">
                    <div class="image_container">
                        <img src="{{ asset('images/repair/repair-calc-image.jpg') }}" alt="">
                    </div>
                    <div class="text_info wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.1s">
                        <h3 class="title ">Получите бесплатный расчет</h3>
                        <p class="text">
                        Консультация со специалистом. Помощь в выборе технологий и материалов. Личная встреча.
                        </p>
                    </div>
                </div>
                <form class="form_cont contact_send" action="{{ route('building.send') }}" method="post">
                    <fieldset class="material wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.2s">
                        <input class="input_cont" type="text" required name="name">
                        <hr>
                        <label>Как Вас зовут?</label>
                    </fieldset>
                    <fieldset class="material wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                        <input class="input_cont number" type="text" required name="phone">
                        <hr>
                        <label>Ваш номер телефона?</label>
                    </fieldset>
                    <div class="custom_btn black wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.4s">
                        <button class="my_btn black" type="submit">
                            <span>Отправить</span>
                            <img src="{{ asset('images/contacts/submit-btn-arrow.svg') }}" alt="">
                        </button>
                    </div>
                    {{ csrf_field() }}
                    <input type="hidden" name="type" value="Ремонт">
                </form>
                <div class="custom_btn wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                    <a class="my_btn portf" href="{{ route('portfolio') }}"><span>Портфолио</span></a>
                </div>
            </div>
        </section>
        <section class="about_bottom">
            <div class="container">
                <div class="section_title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0s" style="display:none">
                    <h2 class="title">ПОДРОБНЕЕ</h2>
                </div>
                <div class="text_block wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.15s">
                    <div class="text"><h2 style="text-align: center;">Ремонт квартир в Одессе от компании&nbsp;Attic Stroy</h2>
<p>Ремонт в квартире &ndash; это возможность самостоятельно придать помещениям индивидуальный стиль и повысить комфорт жилья. Профессионалы компании Attic Stroy выполняют проекты &laquo;под ключ&raquo; с учетом предпочтений клиентов.</p>
<h2 style="text-align: center;">Кому подойдет?</h2>
<p>Ремонт квартиры &ndash; это ответственная работа, требующая тщательной разработки проекта и составления сметы. Услугу заказывают такие клиенты:</p>
<ul>
<li>Владельцы квартир в новостройках. При выборе наших услуг вы можете создать уютное жилье, которое будет полностью отвечать вашим требованиям комфорта.</li>
<li>Владельцы вторичной недвижимости. После приобретения квартиры на вторичном рынке желание сменить дизайн помещений появляется у многих покупателей. Мы предоставляем вам возможность сделать жилье</li>
<li>Владельцам гостиниц. Выполняя ремонт в объектах коммерческой недвижимости, мы в точности соблюдаем сроки работ, что позволяет вам следовать графику.</li>
</ul>
<p>Специалисты компании Attic Stroy выполняют ремонт квартир в Одессе недорого, реализуя проекты любой сложности. В процессе работ применяем современные технологические и стилистические решения.</p>
<h2 style="text-align: center;">Этапы работ</h2>
<p>Каждый дизайн-проект, созданный нашими специалистами, проходит аудит инженерами компании. Это необходимо для определения правильности расположения сантехнического оборудования и технической возможности выполнения поставленных задач</p>
<p>Мы выполняем ремонт квартир в Одессе под ключ более 10 лет, поэтому точно знаем, сколько времени понадобится на проведение работ. На создание проекта, установку сантехники, монтаж теплых полов, шумоизоляцию стен, проведение отделки и пр. понадобится от 2 до 6 месяцев.</p>
<h3 style="text-align: center;">Гарантии</h3>
<p>На ремонт квартир мы предоставляем длительную гарантию &ndash; до 5 лет. Особенности нашей работы:</p>
<ul>
<li>мы берем на себя взаимодействие с управляющей компанией;</li>
<li>строго следуем установленной смете;</li>
<li>предлагаем детальный отчет о проделанной работе на каждом этапе ремонта;</li>
<li>выполняем только комплексный ремонт квартир в Одессе, обеспечивая создание индивидуального стиля жилья и сохранение композиции помещений;</li>
</ul>
<p>Мы предлагаем максимальную степень проработки интерьера нашими дизайнерами. Поскольку мы выполняем ремонт только с использованием проверенных материалов, вы можете быть уверены в качестве отделки полов, потолка и стен в любой комнате.</p>
<h3 style="text-align: center;">Что мы предлагаем?</h3>
<p>Мы создаем оригинальный дизайн квартир с учетом предпочтений владельца. Наши услуги:</p>
<ul>
<li>создание индивидуальной мебели;</li>
<li>составление дизайн-проекта;</li>
<li>перепланировка помещений;</li>
<li>выполнение стяжки;</li>
<li>оштукатуривание поверхностей;</li>
<li>изготовление дверей и окон на заказ;</li>
<li>монтаж натяжных потолков;</li>
<li>проведение электромонтажных и сантехнических работ;</li>
<li>финальная уборка и вывоз мусора.</li>
</ul>
<p>Выбор комплексной услуги позволяет сэкономить время.</p>
<h3 style="text-align: center;">Преимущества</h3>
<p>Повышая клиентский сервис, компания Attic Stroy успешно конкурирует на строительном рынке Одессы с другими застройщиками. Наши преимущества:</p>
<ul>
<li>гарантия на ремонт квартир по договору &ndash; 5 лет;</li>
<li>предоставление консультации &ndash; как сэкономить 20% на стоимости материалов;</li>
<li>помощь в закупке и доставке материалов;</li>
<li>выполнение ремонта любой сложности;</li>
<li>личная встреча со специалистом и бесплатный расчет стоимости работ;</li>
</ul>
<p>Компания Attic Stroy, занимаясь ремонтом квартир в Одессе, гарантирует выполнение всех работ по проекту точно в срок и в полном объеме.</p>
<h2 style="text-align: center;">Заказать ремонт квартиры</h2>
<p>Заказывайте ремонт и отделку квартиры у нас, чтобы сэкономить бюджет и получить длительную гарантию. Уточнить подробности сотрудничества и цены можно по телефонам, указанным на сайте. Напишите нам на электронную почту или оставьте заявку через форму обратной связи.</p>
                    </div>
                </div>
                <div class="custom_btn show_more black wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">
                    <div class="my_btn"><span>Подробнее</span></div>
                </div>
            </div>
        </section>
    </main>

    <div class="modal fade stages_modal" id="service_furniture" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Подробно об услуге</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('images/about/icon-modal-close.png') }}" alt="">
                </button>
            </div>
            <div class="modal-body">
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Производство корпусной мебели</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Производство корпусной мебели включает в себя изделия из дсп, мдф, фанеры и других листовых материалов. 
                                Это могут быть столы, тумбы, комоды, кухни, шкафы-купе, рецепции, спальные гарнитуры, гостиные гарнитуры, гардеробные, прихожие, ванные гарнитуры, стеновые панели не стандартные и другие изделия. 
                                При этом нами используются все доступные в Украине материалы и фурнитура.
                            </p>
                        </div>
                        <div class="row m-0 images_row">
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv1.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv2.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Производство столярных изделий</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Столярное производство - это всегда индивидуальное изделие специально для  каждого клиента. 
                                Наши возможности позволяют изготавливать изделия из дерева любой сложности.
                            </p>
                        </div>
                        <div class="row m-0 images_row">
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv3.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv4.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv5.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Производство мягких изделий</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Наша компания в том числе комплектует свои изделия мягкими элементами из поролона и ткани. Это стеновые панели, изголовья кроватей, кровати, пуфы и тому подобное.
                            </p>
                        </div>
                        <div class="row m-0 images_row">
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv6.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv7.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv8.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Изделия из стекла</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                По желанию заказчика могут проектироваться и изготавливаться изделия из стекла и зеркала, такие как кухонный фартук (скинали) из калёного стекла, поверхности столов и столешницы из калёного стекла, стеклянные двери в душевые кабины, зеркала, зеркальные панно, витражи и витражные перегородки.
                            </p>
                        </div>
                        <div class="row m-0 images_row">
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv9.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv10.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv11.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv12.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Металлоконструкции и изделия в стиле «Лофт»</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Ещё одной стороной нашей компании является изготовление изделий из чёрных и цветных металлом и изделий в стиле «лофт», то есть сочетание таких материалов как металл, стекло и натуральное дерево.
                            </p>
                        </div>
                        <div class="row m-0 images_row">
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv13.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Искусственный камень и кварц</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Производственные мощности позволяют изготавливать изделия из искусственного камня и кварца, а именно кухонные столешницы, кухонные раковины, барные стойки, подоконники, поверхности столов и другие изделия.
                            </p>
                        </div>
                        <div class="row m-0 images_row">
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv14.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv15.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Эксклюзивные изделия</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Нашим преимуществом перед конкурентами является возможность и желание изготовления эксклюзивных и не стандартных изделий, сочетая самые разные материалы в одном изделии.
                            </p>
                        </div>
                        <div class="row m-0 images_row">
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv16.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv17.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="image_container">
                                    <img src="{{ asset('images/repair/services/furniture/loft/serv18.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade stages_modal" id="service_putty" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="height: auto">
            <div class="modal-header">
                <h2 class="modal-title">Подробно об услуге</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('images/about/icon-modal-close.png') }}" alt="">
                </button>
            </div>
            <div class="modal-body">
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Шпатлевка</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Шпатлевка, покраска стен и потолков внутри помещения это работы с использованием технологии механизированного нанесения и шлифовки материала, для чего используется специальное оборудование. 
                                Новая технология обеспечивает высочайшее качество отделки практически любых поверхностей, недостижимое традиционными методами и практически полное отсутствие пыли. 
                                Это позволяет более чем втрое сократить время, затрачиваемое на выполнение подобных работ, при одновременном повышении их качества и сокращении количества материалов. 
                                В конечном итоге, даёт существенную экономию заказчику.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade stages_modal" id="service_insulation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Подробно об услуге</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('images/about/icon-modal-close.png') }}" alt="">
                </button>
            </div>
            <div class="modal-body">
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Проблема новых квартир</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                По анализу и статистике особенно страдают от недостаточной звукоизоляции современные монолитно-каркасные многоэтажки.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Выявление слабых мест</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                В 90% случаев необходим акустический анализ помещения, который производится нашим специалистом совершенно бесплатно.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h2 class="title">Гарантированный результат</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Больше никакого шума, акустического комфорта для вас и ваших близких в короткие сроки.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h2 class="title">Полный комплекс услуг по звукоизоляции и акустике помещения</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Мы проводим анализ и выявляем главные причины шума.
                                Материалы для строительства в современных многоэтажках обладают крайне низким коэффициентом звукоизоляции и отличаются повышенной звукопроводностью, 
                                а каркас здания из металлической арматуры является проводником структурного и ударного шума. <br>
                                С помощью современных технологий мы находим и устраняем причину шума, который мешает тишине и комфорту в доме. <br>
                                Мы работаем только с качественными и экологически чистыми материалами. <br>
                                Мы ценим ваше время и выполняем работы согласно обусловленных договором срокам. <br>
                                Мы всегда удерживаем минимальную цену среди возможных конкурентов, а наши расценки на работы доступны для всех. <br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection