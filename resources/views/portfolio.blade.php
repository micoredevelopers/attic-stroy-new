@extends('layouts.app')
@section('title', 'Портфолио')
@section('content')
    <main id="wrapper_portfolio">
    
        <div class="section_top_info">
        <div itemscope="" itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" style="margin-top:0;margin-bottom:10px; transform:translateZ(1px);color:#fff">
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a rel="nofollow" itemprop="item" title="Строительная компания в Одессе" href="{{ route('index') }}">
          <span itemprop="name" style="color:#fff">Строительная компания в Одессе</span>
          <meta itemprop="position" content="1">
       </a>
   </span> > 
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a itemprop="item" title="Портфолио" href="{{ route('portfolio') }}">
          <span itemprop="name" style="color:#fff">Портфолио</span>
          <meta itemprop="position" content="2">
       </a>
   </span>
</div>
            <div class="container">
            
                <div class="row m-0">
                    <div class="col-xl-6 col-12 p-0">
                        <div class="section_title wow animated fadeInDown" data-wow-duration="0.6s" data-wow-delay="0.1s">
                            <h1 class="title">Портфолио</h1>
                        </div>
                    </div>
                    <div class="col-xl-6 col-12 p-0">
                        <div class="text_info">
                            <p class="text bold wow animated fadeInDown" data-wow-duration="0.6s" data-wow-delay="0.2s">Успешно реализованные проекты команды Attic Stroy</p>
                            <p class="text wow animated fadeInDown" data-wow-duration="0.6s" data-wow-delay="0.3s">
                                На этой странице представлены некоторые из сданных проектов нашей компании. Вы можете ознакомиться со стоимостью, сроком работы и увидеть, как реализовываются дизайнерские идеи.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="portfolio_container">
                <!-- <div class="btn_select_row">
                    <div class="custom_btn build" data-id="build">
                        <div class="my_btn wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.35s"><span>Строительство</span></div>
                    </div>
                    <div class="custom_btn isShow repair" data-id="repair">
                        <div class="my_btn wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.35s"><span>Ремонт</span></div>
                    </div>
                </div> -->
                <div class="all_block_cont">


                    <div id="build" class="row block_row m-0">
                        @foreach($portfolios_build as $build)
                                <div class="col-md-6 col-12 p-0">
                                    <a href="{{ route('portfolio.show', $build->id) }}">
                                    <div class="portfolio_info">
                                        <div class="portfolio_image wow animated fadeInUp" data-wow-duration="0.6s">
                                            @isset($build->main_photo)
                                                @php $build->main_photo = json_decode($build->main_photo) @endphp
                                                <img src=" {{ Storage::disk('upload')->url($build->main_photo[1]) }}" alt="">
                                            @endisset
                                        </div>
                                        <div class="portfolio_text">
                                            <h5 class="title wow animated fadeInUp" data-wow-duration="0.6s">{{ $build->title ?? '' }}</h5>
                                            <p class="text wow animated fadeInUp">{{ $build->desc ?? '' }}</p>
                                        </div>
                                    </div>
                                    </a>
                                </div>

                        @endforeach
                    </div>

                    <div id="repair" class="row block_row m-0 isShow">
                        @foreach($portfolios_repair as $repair)
                                <div class="col-md-6 col-12 p-0">
                                    <a href="{{ route('portfolio.show', $repair->id) }}">
                                    <div class="portfolio_info">
                                        <div class="portfolio_image wow animated fadeInUp" data-wow-duration="0.6s">
                                            @isset($repair->main_photo)
                                                @php $repair->main_photo = json_decode($repair->main_photo) @endphp
                                                <img src=" {{ Storage::disk('upload')->url($repair->main_photo[1]) }}" alt="">
                                            @endisset
                                        </div>
                                        <div class="portfolio_text">
                                            <h5 class="title wow animated fadeInUp" data-wow-duration="0.6s">{{ $repair->title ?? '' }}</h5>
                                            <p class="text wow animated fadeInUp" data-wow-duration="0.6s">{{ $repair->desc ?? '' }}</p>
                                        </div>
                                    </div>
                                    </a>
                                </div>

                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

