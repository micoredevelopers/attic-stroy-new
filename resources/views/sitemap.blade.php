@php
 echo '<?xml version="1.0" encoding="UTF-8"?>'; 
@endphp
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://www.attic-stroy.com</loc>
    </url>
    <url>
        <loc>https://www.attic-stroy.com/about</loc>
    </url>
    <url>
        <loc>https://www.attic-stroy.com/repair</loc>
    </url>
    <url>
        <loc>https://www.attic-stroy.com/building</loc>
    </url>
    <url>
        <loc>https://www.attic-stroy.com/portfolio</loc>
    </url>
    
    @foreach($portfolios_build as $build)
        <url>
            <loc>{{ route('portfolio.show', $build->id) }}</loc>
        </url>
    @endforeach
    <url>
        <loc>https://www.attic-stroy.com/contacts</loc>
    </url>
</urlset>
