<header class="main active">
    <div class="container">
        @if (url()->current() == route('repair'))
            <div class="header_container half mobile d-flex d-xl-none">
                <div class="menu_icon">
                    <span></span>
                    <span></span>
                </div>
                <div class="logo_container">
                    <a href="{{route('index')}}">
                        <img src="{{ asset('images/main/logo_black.svg') }}" alt="" class="logo">
                    </a>
                </div>
                <div class="phone_icon">
                    <a href="tel:{{ $tel_1 ?? '' }}">
                        <img src="{{ asset('images/main/phone_white.svg') }}" alt="" class="phone white">
                        <img src="{{ asset('images/main/phone_black.svg') }}" alt="" class="phone black">
                    </a>
                </div>
            </div>
            <div class="header_container half desktop d-none d-xl-flex">
                <div class="logo_container">
                    <a href="{{ route('index') }}">
                        <img src="{{ asset('images/main/logo_black.svg') }}" alt="" class="logo black">
                    </a>
                </div>
                <div class="right_header_block">
                    <div class="custom_btn">
                        <div class="my_btn"><span data-toggle="modal" data-target="#feedback_modal">Связаться с нами</span></div>
                    </div>
                    <div class="phone_row">
                        <a class="phone_block" href="tel:{{ $tel_1 ?? '' }}" style="color:black;">
                            <img src="{{ asset('images/main/phone_black.svg') }}" alt="" class="phone white">
                            <img src="{{ asset('images/main/phone_black.svg') }}" alt="" class="phone black">
                            {{ $contact->phone_1 ?? '' }}
                        </a>
                        <a class="phone_block" href="tel:{{ $tel_2 ?? '' }}" style="color:black">
                            <img src="{{ asset('images/main/phone_black.svg') }}" alt="" class="phone white">
                            <img src="{{ asset('images/main/phone_black.svg') }}" alt="" class="phone black">
                            {{ $contact->phone_2 ?? '' }}
                        </a>
                    </div>
                    <div class="menu_icon white">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        @else
            <div class="header_container mobile black d-flex d-xl-none">
                <div class="menu_icon">
                    <span></span>
                    <span></span>
                </div>
                <div class="logo_container">
                    <a href="{{ route('index') }}">
                        <img src="{{ asset('images/main/logo_black.svg') }}" alt="" class="logo">
                    </a>
                </div>
                <div class="phone_icon">
                    <a href="tel:{{ $tel_1 ?? '' }}">
                        <img src="{{ asset('images/main/phone_black.svg') }}" alt="" class="phone black">
                    </a>
                </div>
            </div>
            <div class="header_container mobile white d-flex d-xl-none">
                <div class="menu_icon">
                    <span></span>
                    <span></span>
                </div>
                <div class="logo_container">
                    <a href="{{ route('index') }}">
                        <img src="{{ asset('images/main/logo_white.svg') }}" alt="" class="logo">
                    </a>
                </div>
                <div class="phone_icon">
                    <a href="tel:{{ $tel_1 ?? '' }}">
                        <img src="{{ asset('images/main/phone_white.svg') }}" alt="" class="phone white">
                    </a>
                </div>
            </div>

            <div class="header_container desktop white d-none d-xl-flex">
                <div class="logo_container">
                    <a href="{{ route('index') }}">
                        <img src="{{ asset('images/main/logo_white.svg') }}" alt="" class="logo white">
                    </a>
                </div>
                <div class="right_header_block">
                    <div class="custom_btn">
                        <div class="my_btn" data-toggle="modal" data-target="#feedback_modal"><span>Связаться с нами</span></div>
                    </div>
                    <div class="phone_row">
                        <a class="phone_block" href="tel:{{ $tel_1 ?? '' }}">
                            <img src="{{ asset('images/main/phone_white.svg') }}" alt="" class="phone white">
                            {{ $contact->phone_1 ?? '' }}
                        </a>
                        <a class="phone_block" href="tel:{{ $tel_2 ?? '' }}">
                            <img src="{{ asset('images/main/phone_white.svg') }}" alt="" class="phone white">
                            {{ $contact->phone_2 ?? '' }}
                        </a>
                    </div>
                    <div class="menu_icon">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="header_container desktop black d-none d-xl-flex">
                <div class="logo_container">
                    <a href="{{ route('index') }}">
                        <img src="{{ asset('images/main/logo_black.svg') }}" alt="" class="logo black">
                    </a>
                </div>
                <div class="right_header_block">
                    <div class="custom_btn">
                        <div class="my_btn" data-toggle="modal" data-target="#feedback_modal"><span>Связаться с нами</span></div>
                    </div>
                    <div class="phone_row">
                        <a class="phone_block" href="tel:{{ $tel_1 ?? '' }}">
                            <img src="{{ asset('images/main/phone_black.svg') }}" alt="" class="phone black">
                            {{ $contact->phone_1 ?? '' }}


                        </a>
                        <a class="phone_block" href="tel:{{ $tel_2 ?? '' }}">
                            <img src="{{ asset('images/main/phone_black.svg') }}" alt="" class="phone black">
                            {{ $contact->phone_2 ?? '' }}
                        </a>
                    </div>
                    <div class="menu_icon">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        @endif
    </div>
</header>

<div class="menu_container mobile">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="col-xl-6 col-12 p-0">
                <div class="nav_container">
                    <ul class="nav_list">
                        <li class="list_item"><a href="{{ route('about') }}" class="list_link">О компании</a></li>
                        <li class="list_item"><a href="{{ route('building') }}" class="list_link">Строительство</a></li>
                        <li class="list_item"><a href="{{ route('repair') }}" class="list_link">Ремонт</a></li>
                        <li class="list_item"><a href="{{ route('portfolio') }}" class="list_link">Портфолио</a></li>
                        <li class="list_item"><a href="{{ route('contacts') }}" class="list_link">Контакты</a></li>
                    </ul>
                    <div class="contact_data d-none d-sm-flex">
                        <a href="tel:{{ $tel_1 ?? '' }}" class="text number">
                        <img src="{{ asset('images/about/location-icon.svg') }}" alt="">г. Одесса, французкий бульвар 66/1
                            <!-- <img src="{{ asset('images/main/phone_default.svg') }}" alt="">{{ $contact->phone_1 ?? '' }} -->
                        </a>
                        <a href="mail:{{ $contact->email ?? '' }}" class="text mail">
                            <img src="{{ asset('images/main/mail_default.svg') }}" alt="">{{ $contact->email ?? '' }}
                        </a>
                    </div>
                </div>
                <div class="bottom_socials">
                    <a href="{{ $contact->link_f ?? '' }}" target="_blank">
                        <div class="social_cont">
                            <div class="icon facebook">
                                <!--                                --><?//xml version="1.0" encoding="UTF-8"?>
                                <svg width="13px" height="25px" viewBox="0 0 13 25" version="1.1"
                                     xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <title>social-facebook - simple-line-icons</title>
                                    <desc>Created with Sketch.</desc>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="Меню-XL" transform="translate(-106.000000, -971.000000)" fill="#201600">
                                            <g id="Group-16-Copy-2" transform="translate(60.000000, 957.000000)">
                                                <path
                                                    d="M54.640625,16.375 C54.3281234,16.375 53.7226607,16.5351546 52.8242188,16.8554688 C51.9257768,17.1757829 51.4765625,18.1015548 51.4765625,19.6328125 L51.4765625,23.125 L48.4765625,23.125 L48.4765625,25.375 L51.4765625,25.375 L51.4765625,36.625 L53.7265625,36.625 L53.7265625,25.375 L56.984375,25.375 L57.5234375,23.125 L53.7265625,23.125 L53.7265625,19.5625 L53.8554688,19.1054688 C53.9414067,18.8007797 54.2499973,18.6484375 54.78125,18.6484375 L56.7265625,18.6484375 L56.7265625,16.3984375 L54.78125,16.3984375 L54.7460938,16.3867188 C54.7226561,16.3789062 54.6875002,16.375 54.640625,16.375 Z M54.640625,14.875 C54.6718752,14.875 54.7031248,14.8789062 54.734375,14.8867188 C54.7656252,14.8945313 54.7968748,14.8984375 54.828125,14.8984375 L56.75,14.8984375 C57.156252,14.8984375 57.5039048,15.0429673 57.7929688,15.3320312 C58.0820327,15.6210952 58.2265625,15.9765604 58.2265625,16.3984375 C58.2265625,16.4140626 58.2265625,16.4257812 58.2265625,16.4335938 C58.2265625,16.4414063 58.2265625,16.4531249 58.2265625,16.46875 L58.2265625,18.6484375 C58.2265625,19.0546895 58.0781265,19.4062485 57.78125,19.703125 C57.4843735,20.0000015 57.1328145,20.1484375 56.7265625,20.1484375 L55.2265625,20.1484375 L55.2265625,21.625 L57.5234375,21.625 C57.7578137,21.625 57.9804677,21.6757807 58.1914062,21.7773438 C58.4023448,21.8789068 58.5781243,22.031249 58.71875,22.234375 C58.8593757,22.4218759 58.9531248,22.6289051 59,22.8554688 C59.0468752,23.0820324 59.0312504,23.3124988 58.953125,23.546875 L58.4375,25.796875 C58.3437495,26.1093766 58.1640638,26.3671865 57.8984375,26.5703125 C57.6328112,26.7734385 57.3281267,26.875 56.984375,26.875 L55.2265625,26.875 L55.2265625,36.6015625 C55.2265625,37.0234396 55.0781265,37.3789048 54.78125,37.6679688 C54.4843735,37.9570327 54.1328145,38.1015625 53.7265625,38.1015625 L51.4765625,38.1015625 C51.0546854,38.1015625 50.6992202,37.9570327 50.4101562,37.6679688 C50.1210923,37.3789048 49.9765625,37.0234396 49.9765625,36.6015625 L49.9765625,26.875 L48.4765625,26.875 C48.0703105,26.875 47.7187515,26.726564 47.421875,26.4296875 C47.1249985,26.132811 46.9765625,25.781252 46.9765625,25.375 L46.9765625,23.125 C46.9765625,22.921874 47.0156246,22.7304696 47.09375,22.5507812 C47.1718754,22.3710929 47.2812493,22.2109382 47.421875,22.0703125 C47.5625007,21.9296868 47.7226554,21.8203129 47.9023438,21.7421875 C48.0820321,21.6640621 48.2734365,21.625 48.4765625,21.625 L49.9765625,21.625 L49.9765625,19.6328125 C49.9765625,18.4765567 50.2031227,17.5820344 50.65625,16.9492188 C51.1093773,16.3164031 51.5703102,15.8593764 52.0390625,15.578125 C52.539065,15.2812485 53.0312476,15.0898442 53.515625,15.0039062 C54.0000024,14.9179683 54.3749987,14.875 54.640625,14.875 Z"
                                                    id="social-facebook---simple-line-icons"></path>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </a>
                    <a href="{{ $contact->link_i ?? '' }}" target="_blank">
                        <div class="social_cont">
                            <div class="icon instagram">
                                <!--                                --><?//xml version="1.0" encoding="UTF-8"?>
                                <svg width="21px" height="21px" viewBox="0 0 21 21" version="1.1"
                                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <title>instagram - FontAwesome</title>
                                    <desc>Created with Sketch.</desc>
                                    <g id="Page-2" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="Меню-XL2" transform="translate(-260.000000, -973.000000)" fill="#201600">
                                            <g id="Group-16-Copy-3" transform="translate(217.000000, 957.000000)">
                                                <path
                                                    d="M56.7143021,26.4285612 C56.7143021,24.5401661 55.1741217,22.9999857 53.2857265,22.9999857 C51.3973314,22.9999857 49.857151,24.5401661 49.857151,26.4285612 C49.857151,28.3169563 51.3973314,29.8571367 53.2857265,29.8571367 C55.1741217,29.8571367 56.7143021,28.3169563 56.7143021,26.4285612 Z M58.5625186,26.4285612 C58.5625186,29.3482075 56.2053729,31.7053532 53.2857265,31.7053532 C50.3660802,31.7053532 48.0089345,29.3482075 48.0089345,26.4285612 C48.0089345,23.5089149 50.3660802,21.1517692 53.2857265,21.1517692 C56.2053729,21.1517692 58.5625186,23.5089149 58.5625186,26.4285612 Z M60.0089488,20.9374832 C60.0089488,21.6205198 59.4598411,22.1696276 58.7768045,22.1696276 C58.093768,22.1696276 57.5446602,21.6205198 57.5446602,20.9374832 C57.5446602,20.2544467 58.093768,19.7053389 58.7768045,19.7053389 C59.4598411,19.7053389 60.0089488,20.2544467 60.0089488,20.9374832 Z M53.2857265,17.9910512 C51.7857248,17.9910512 48.5714352,17.8705153 47.218755,18.4062302 C46.7500045,18.5937304 46.4017898,18.8214093 46.0401822,19.1830169 C45.6785746,19.5446244 45.4508958,19.8928391 45.2633956,20.3615897 C44.7276806,21.7142699 44.8482165,24.9285594 44.8482165,26.4285612 C44.8482165,27.928563 44.7276806,31.1428525 45.2633956,32.4955327 C45.4508958,32.9642833 45.6785746,33.312498 46.0401822,33.6741056 C46.4017898,34.0357131 46.7500045,34.263392 47.218755,34.4508922 C48.5714352,34.9866071 51.7857248,34.8660713 53.2857265,34.8660713 C54.7857283,34.8660713 58.0000179,34.9866071 59.3526981,34.4508922 C59.8214486,34.263392 60.1696633,34.0357131 60.5312709,33.6741056 C60.8928785,33.312498 61.1205573,32.9642833 61.3080575,32.4955327 C61.8437725,31.1428525 61.7232366,27.928563 61.7232366,26.4285612 C61.7232366,24.9285594 61.8437725,21.7142699 61.3080575,20.3615897 C61.1205573,19.8928391 60.8928785,19.5446244 60.5312709,19.1830169 C60.1696633,18.8214093 59.8214486,18.5937304 59.3526981,18.4062302 C58.0000179,17.8705153 54.7857283,17.9910512 53.2857265,17.9910512 Z M63.5714531,26.4285612 C63.5714531,27.8482058 63.584846,29.2544574 63.5044887,30.674102 C63.4241315,32.3214254 63.049131,33.7812485 61.8437725,34.9866071 C60.6384139,36.1919657 59.1785907,36.5669662 57.5312673,36.6473234 C56.1116228,36.7276806 54.7053711,36.7142878 53.2857265,36.7142878 C51.866082,36.7142878 50.4598303,36.7276806 49.0401858,36.6473234 C47.3928624,36.5669662 45.9330392,36.1919657 44.7276806,34.9866071 C43.5223221,33.7812485 43.1473216,32.3214254 43.0669644,30.674102 C42.9866071,29.2544574 43,27.8482058 43,26.4285612 C43,25.0089167 42.9866071,23.602665 43.0669644,22.1830204 C43.1473216,20.535697 43.5223221,19.0758739 44.7276806,17.8705153 C45.9330392,16.6651567 47.3928624,16.2901563 49.0401858,16.209799 C50.4598303,16.1294418 51.866082,16.1428347 53.2857265,16.1428347 C54.7053711,16.1428347 56.1116228,16.1294418 57.5312673,16.209799 C59.1785907,16.2901563 60.6384139,16.6651567 61.8437725,17.8705153 C63.049131,19.0758739 63.4241315,20.535697 63.5044887,22.1830204 C63.584846,23.602665 63.5714531,25.0089167 63.5714531,26.4285612 Z"
                                                    id="instagram---FontAwesome"></path>
                                            </g>
                                        </g>
                                    </g>
                               </svg>
                            </div>
                        </div>
                    </a>
                    <div class="custom_btn d-flex d-xl-none">
                        <div class="my_btn" data-toggle="modal" data-target="#feedback_modal"><span>Связаться с нами</span></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 p-0 d-none d-xl-flex">
                <div class="image_cont">
                    <img src="{{ asset('images/main/menu-desktop-image.JPG') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal success -->
<div class="modal fade" id="success_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Ваше сообщение отправлено, мы скоро с Вами свяжемся!</h2>
            </div>
            <div class="modal-footer">
                <div class="custom_btn black">
                    <button class="my_btn" data-dismiss="modal">Окей</button>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal packages -->
<div class="modal fade packages_modal" id="modal_standart" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Подробно о пакетах</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('images/about/icon-modal-close.png') }}" alt="">
                </button>
            </div>
            <div class="modal-body">
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Электрика</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Прокладка кабельных трасс розеточной сети и сети освещения. <br>
                                - Монтаж розеток в каждом помещении на высоте 300 мм от пола. <br>
                                - Монтаж выключателей в каждом помещении на высоте 1050 мм от пола. <br>
                                - Установка подрозетников, сборка и монтаж силового щита. <br>
                                - Установка на механизмы розеток и выключателей рамок, накладоки клавиш. <br>
                                - Установка накладных светильников и люстр.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Стены</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Штукатурка стен машинного нанесения по немецкой технологии используя грунты 4-го поколения. <br>
                                - Механизированная шпаклёвка стен. <br>
                                - Монтаж пластиковых подоконников. <br>
                                - Поклейка обоев. <br>
                                - Шпаклёвка и безвоздушная покраска откосов. <br>
                                - Монтаж плитки (сан. узла и кухонного фартука). <br>
                                - Установка межкомнатных дверей с ручками и наличниками.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Сантехника</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Разводка горячей, холодной воды и канализации от стояков к местам установки сантехники. <br>
                                - Установка сантехнических приборов: ванна, унитаз, мойдодыр, зеркало,полотенцесушитель, бойлер и комплект смесителей.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Потолки</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Монтаж натяжного потолка. <br>
                                Монтаж подшторных ниш. <br>
                                Механизированная шпаклевка ГКЛ. <br>
                                Безвоздушная покраска потолка. <br>
                                Поклейка и покраска багета.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Полы</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Монтаж ламината. <br>
                                Монтаж плитки (коридор, сан. узел, кухня). <br>
                                Монтаж плинтуса.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Подготовка к сдаче квартиры</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Вывоз мусора во время ремонта и после окончания работ. <br>
                                - Строительная уборка. <br>
                                - Чистовая уборка
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade packages_modal" id="modal_comfort" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Подробно о пакетах</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('images/about/icon-modal-close.png') }}" alt="">
                </button>
            </div>
            <div class="modal-body">
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="left_block">
                            <div class="title_cont">
                                <h3 class="title">Электрика</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Прокладка кабельных трасс розеточной сети и сети освещения. <br>
                                - Монтаж розеток в каждом помещении на высоте 300 мм от пола. <br>
                                - Монтаж выключателей в каждом помещении на высоте 1050 мм от пола. <br>
                                - Установка подрозетников, сборка и монтаж силового щита. <br>
                                - Установка на механизмы розеток и выключателей рамок, накладоки клавиш. <br>
                                - Установка накладных светильников и люстр.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Стены</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Штукатурка стен машинного нанесения по немецкой технологии используя грунты 4-го поколения. <br>
                                - Механизированная шпаклёвка стен. <br>
                                - Монтаж пластиковых подоконников. <br>
                                - Поклейка обоев. <br>
                                - Шпаклёвка и безвоздушная покраска откосов. <br>
                                - Монтаж плитки (сан. узла и кухонного фартука). <br>
                                - Установка межкомнатных дверей с ручками и наличниками.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Сантехника</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Разводка горячей, холодной воды и канализации от стояков к местам установки сантехники. <br>
                                - Установка сантехнических приборов: ванна, унитаз, мойдодыр, зеркало,полотенцесушитель, бойлер и комплект смесителей.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Потолки</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Монтаж натяжного потолка. <br>
                                Монтаж подшторных ниш. <br>
                                Механизированная шпаклевка ГКЛ. <br>
                                Безвоздушная покраска потолка. <br>
                                Поклейка и покраска багета.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Полы</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                Монтаж ламината. <br>
                                Монтаж плитки (коридор, сан. узел, кухня). <br>
                                Монтаж плинтуса.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Климатическая установка</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Монтаж кондиционера (от 2 шт.)
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Шторы</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Пошив штор по индивидуальному заказу.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Кухня под заказ</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Корпус ДСП Egger. <br>
                                - Ламинированные МДФ фасады. <br>
                                - Фурнитура Blum. <br>
                                - Рабочая поверхность ДСП Egger.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Спальный гарнитур</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Кровать (2000х1800 мм). <br>
                                - Прикроватная тумбочка (2 шт.). <br>
                                - Шкаф купэ.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-5 col-12 p-0">
                        <div class="title_cont">
                            <h3 class="title">Подготовка к сдаче квартиры</h3>
                        </div>
                    </div>
                    <div class="col-sm-7 col-12 p-0">
                        <div class="text_info">
                            <p class="text">
                                - Вывоз мусора во время ремонта и после окончания работ. <br>
                                - Строительная уборка. <br>
                                - Чистовая уборка
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row detail_block m-0">
                    <div class="col-sm-12 col-12 p-0">
                        <div class="text_info">
                            <p class="text last_info">
                                Отделка балкона предоставляется отдельным коммерческим предложением.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal feedback -->
<div class="modal fade feedback_modal" id="feedback_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Бесплатная консультация в Одессе</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('images/about/icon-modal-close.png') }}" alt="">
                </button>
            </div>
            <div class="modal-body">
                <form class="form_cont contact_send" action="{{ route('contact.send') }}" method="post">
                    <div class="form-group">
                        <input type="text" placeholder="Имя" name="name">
                    </div>
                    <div class="form-group">
                        <input type="tel" placeholder="Телефон" name="phone">
                    </div>
                    <div class="form-group">
                        <input type="email" placeholder="Почта" name="email">
                    </div>
                    <div class="custom_btn black">
                        <button type="submit" class="my_btn black">
                            Оставить заявку
                            <img src="{{ asset('images/contacts/submit-btn-arrow.svg') }}" alt="">
                        </button>
                    </div>
                    {{ csrf_field() }}
                    <input type="hidden" name="type" value="{{ Request::url() }}">
                    <div class="error_text">
                        <p class="text">Ошибка при отправке данных</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- The Modal -->
<div id="myModal" class="modalImg">
    <span class="close">&times;</span>
    <img class="modal-content" src="#" alt="" id="img01">
</div>
