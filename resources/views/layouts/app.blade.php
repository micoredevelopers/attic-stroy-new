<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('title')</title>
    <meta name="keywords" content="@yield('meta')">
    <meta name="description" content="@yield('desc')">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-55GGB9V');</script>
<!-- End Google Tag Manager -->
<meta name="google-site-verification" content="kdCpi4xGW30lRAwlZ0I9p9EfH8ptd2ImVu7xmW0F5S0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
{{--<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900&amp;subset=latin-ext"--}}
{{--rel="stylesheet">--}}

<!-- Styles -->
    <!--{{--<link rel="stylesheet" href="styles/bootstrap-grid.min.css">--}}
    {{--<link rel="stylesheet" href="styles/bootstrap-reboot.min.css">--}}
    {{--<link rel="stylesheet" href="styles/bootstrap.min.css">--}}
    {{--<link rel="stylesheet" href="styles/main.css">--}}-->

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-grid.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-reboot.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.min.css') }}">

    <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon">
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "Строительная компания Attic Stroy",
  "url": "https://attic-stroy.com",
  "logo": "https://attic-stroy.com/images/main/logo_black.svg",
  "contactPoint": [{
    "@type": "ContactPoint",
    "telephone": "+380939404404",
    "contactType": "customer service",
    "email": "attic.stroy@gmail.com"
  }],
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Французский бульвар, 66/1",
    "addressLocality": "Одесса",
    "postalCode": "65009",
    "addressCountry": "UA"
  }
  
}
</script>

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-55GGB9V"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="wrapper_all">

    @section('header')
        @include('layouts.header')
    @show
    @yield('content')

    @section('footer')
        @include('layouts.footer')
    @show


</div>


<!-- Scripts -->
<script src="{{ asset('js/jquery-3.3.1.min.js') }}" defer></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}" defer></script>
<script src="{{ asset('js/jquery.browser.js') }}" defer></script>
<script src="{{ asset('js/swiper.min.js') }}" defer></script>
<script src="{{ asset('js/wow.min.js') }}" defer></script>
<script src="{{ asset('js/progressbar.min.js') }}" defer></script>
<script src="{{ asset('js/jquery.maskedinput-1.3.min.js') }}" defer></script>
<script src="https://unpkg.com/imask"></script>
<script src="{{ asset('js/main.js') }}" defer></script>
<script>


</script>
@if (url()->current() == route('repair')|| url()->current() == route('building'))
<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ua/b10419521/crm/site_button/loader_2_vysf44.js');
</script>
@endif
</body>
</html>
