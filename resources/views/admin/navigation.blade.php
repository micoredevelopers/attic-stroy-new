	<header>
			<div class="container-fluid p-0">
				<div class="row m-0">
					<div class="col-12 p-0">
						<div class="nav_container">
							<a href="/" class="logo_container">
							    <img src="{{ asset('images/main/logo_white.svg') }}" alt="">
							</a>
							<ul class="nav_menu">
                                <li class="nav_item">
                                    <a href="{{ route('admin.portfolio.index') }}">
                                        <div class="img_container"><img src="{{ asset('images/admin/briefcase.svg') }}" alt=""></div>
                                        <div class="text"><p>Портфолио</p></div>
                                    </a>
                                </li>
                                <li class="nav_item">
                                    <a href="{{ route('admin.comments.index') }}">
                                        <div class="img_container"><img src="{{ asset('images/admin/briefcase.svg') }}" alt=""></div>
                                        <div class="text"><p>Комментарии</p></div>
                                    </a>
                                </li>
                                <li class="nav_item">
                                    <a href="{{ route('admin.contact.index') }}">
                                        <div class="img_container"><img src="{{ asset('images/admin/contact.svg') }}" alt=""></div>
                                        <div class="text"><p>Контакты</p></div>
                                    </a>
                                </li>
                                <li class="nav_item">
                                    <a href="{{ route('admin.setting') }}">
                                        <div class="img_container"><img src="{{ asset('images/admin/settings_icon.png') }}" alt=""></div>
                                        <div class="text"><p>Настройки</p></div>
                                    </a>
                                </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>
