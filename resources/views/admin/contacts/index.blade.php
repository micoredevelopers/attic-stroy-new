@extends('admin.layouts.app_admin')


@section('wrap-class')
    <div id="wrapper-products">
        @endsection

        @section('navigation')
            @include('admin.navigation')
        @endsection

        @section('content')
            @isset($contact->id)
            <main>
                <div class="section_main">
                    <div class="container-fluid p-0">
                        <div class="row m-0">
                            <div class="col-12 p-0">
                                <div class="header_search">
                                    <div class="title">
                                        <h1>Контакты</h1>
                                    </div>
                                    <div class="add_btn">
                                        <a href="{{route('admin.contact.edit', $contact->id)}}"><img
                                                src="{{ asset('images/admin/edit.svg') }}" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table_main row m-0">
                            <div class="col-12 p-0">
                                <div class="table_container">
                                    <table>
                                        <thead>
                                        <tr>
                                            <th><p>#</p></th>
                                            <th><p>Поле</p></th>
                                            <th><p>Данные</p></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th><p>1</p></th>
                                                <th>
                                                    <p>Телефон №1</p>
                                                </th>

                                                <th> <p>{{ isset($contact->phone_1) ? $contact->phone_1 : ''}}</p></th>
                                            </tr>
                                            <tr>
                                                <th><p>2</p></th>
                                                <th>
                                                    <p>Телефон №2</p>
                                                </th>

                                                <th> <p>{{ isset($contact->phone_2) ? $contact->phone_2 : ''}}</p></th>
                                            </tr>
                                            <tr>
                                                <th><p>3</p></th>
                                                <th>
                                                    <p>Почта</p>
                                                </th>

                                                <th> <p>{{ isset($contact->email) ? $contact->email : ''}}</p></th>
                                            </tr>
                                            <tr>
                                                <th><p>4</p></th>
                                                <th>
                                                    <p>Адресс</p>
                                                </th>
                                                <th> <p>{{ isset($contact->address) ? $contact->address : ''}}</p></th>
                                            </tr>
                                            <tr>
                                                <th><p>5</p></th>
                                                <th>
                                                    <p>Координаты</p>
                                                </th>
                                                <th> <p>{{ isset($contact->map) ? $contact->map : ''}}</p></th>
                                            </tr>
                                            <tr>
                                                <th><p>6</p></th>
                                                <th>
                                                    <p>Ссылка в фейсбук</p>
                                                </th>
                                                <th> <p>{{ isset($contact->link_f) ? $contact->link_f : ''}}</p></th>
                                            </tr>
                                            <tr>
                                                <th><p>7</p></th>
                                                <th>
                                                    <p>Ссылка в инстаграм</p>
                                                </th>
                                                <th> <p>{{ isset($contact->link_i) ? $contact->link_i : ''}}</p></th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                @endisset
            </main>




@endsection
