<div class="row m-0">
    <div class="product_block col-7 p-0">
        <div class="form_product_info">
            <div class="row m-0">
                <div class="col-12 p-0">
                    <div class="photo_video_block">
                        <div class="row m-0">
                            <div class="col-12 p-0">
                                <div class="input_container">
                                    <p class="input_lable">Телефон №1</p>
                                    <input name="phone_1" type="text" placeholder="Телефон №1"
                                           class="@if($errors->first('phone_1')) error @endif"
                                           value="{{ (isset($contact->phone_1)) ? $contact->phone_1 : old('phone_1') }}"
                                    >
                                </div>
                            </div>
                            <div class="col-12 p-0">
                                <div class="input_container">
                                    <p class="input_lable">Телефон №2</p>
                                    <input name="phone_2" type="text" placeholder="Телефон №2"
                                    class="@if($errors->first('phone_2')) error @endif"
                                    value="{{ (isset($contact->phone_2)) ? $contact->phone_2 : old('phone_2') }}"
                                    >
                                </div>
                            </div>

                            <div class="col-12 p-0">
                                <div class="input_container">
                                    <p class="input_lable">Почта</p>
                                    <input name="email" type="email" placeholder="Email"
                                           class="@if($errors->first('email')) error @endif"
                                           value="{{ (isset($contact->email)) ? $contact->email : old('email') }}"
                                           >
                                </div>
                            </div>
                            <div class="col-6 p-0">
                                <div class=" input_container">
                                    <p class="input_lable">Адресс</p>
                                    <input name="address" type="text" placeholder="Адресс"
                                        class="@if($errors->first('address')) error @endif"
                                        value="{{ (isset($contact->address)) ? $contact->address : old('address') }}"
                                    >
                                </div>
                            </div>
                            <div class="col-6 p-0">
                                <div class="input_container">
                                    <p class="input_lable">Координаты</p>
                                    <input name="map" type="text" placeholder="Координаты"
                                        {{--class="@if($errors->first('map')) error @endif"--}}
                                        value="{{ (isset($contact->map)) ? $contact->map : old('map') }}"
                                    >
                                </div>
                            </div>
                            <div class="col-12 p-0">
                                <div class="input_container">
                                    <p class="input_lable">Ссылка в фейсбук</p>
                                    <input name="link_f" type="text" placeholder="facebook"
                                        {{--class="@if($errors->first('name')) error @endif"--}}
                                        value="{{ (isset($contact->link_f)) ? $contact->link_f : old('link_f') }}"
                                    >
                                </div>
                            </div>
                            <div class="col-12 p-0">
                                <div class="input_container">
                                    <p class="input_lable">Ссылка в инстаграм</p>
                                    <input name="link_i" type="text" placeholder="instagram"
                                        {{--class="@if($errors->first('name')) error @endif"--}}
                                        value="{{ (isset($contact->link_i)) ? $contact->link_i : old('link_i') }}"
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="btn_container">
                <button class="custom_btn" type="submit">
                    <span> Изменить контакты </span>
                </button>
            </div>
        </div>

    </div>
</div>
