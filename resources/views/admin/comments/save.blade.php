@extends('admin.layouts.app_admin')

@section('wrap-class')
    <div id="wrapper-product-info">
        @endsection

        @section('navigation')
            @include('admin.navigation')
        @endsection

        @section('content')
            <main>
                <div class="section_main">
                    <div class="container-fluid p-0">
                        <div class="row m-0">
                            <div class="col-12 p-0">
                                <div class="header_search">
                                    <a href="{{ route('admin.comments.index') }}" class="title">
                                        <img src="{{ asset('images/admin/arrow_left.png') }}" alt="">
                                        <span> Комментарий - {{ $comments->exists ? 'изменение' : 'создание' }}  </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_info_container">
                            <form id="create-form" method="post" enctype="multipart/form-data">

                                <div class="row m-0">
                                    <div class="product_block col-7 p-0">
                                        <div class="form_product_info">
                                            <div class="row m-0">
                                                <div class="col-12 p-0">
                                                    <div class="photo_video_block">
                                                        <div class="row m-0">


                                                            <div class="col-12 p-0">
                                                                <div class="input_container">
                                                                    <p class="input_lable">Имя</p>
                                                                    <input name="name" type="text"
                                                                           placeholder="Имя"
                                                                           {{--class="@if($errors->first('phone_1')) error @endif"--}}
                                                                           value="{{ $comments->name }}"
                                                                    >
                                                                </div>
                                                            </div>

                                                            <div class="col-12 p-0">
                                                                <div class="input_container">
                                                                    <p class="input_lable">Должность</p>
                                                                    <input name="position" type="text"
                                                                           placeholder="Должность"
                                                                           value="{{ $comments->position }}"
                                                                    >
                                                                </div>
                                                            </div>

                                                            <div class="col-12 p-0">
                                                                <div class="input_container">
                                                                    <p class="input_lable">Комментарий</p>
                                                                    <textarea name="comment" rows="5">{{ $comments->comment }}</textarea>

                                                                </div>
                                                            </div>
                                                            {{-- todo отобразить текущее фото--}}
                                                            @if(\App\Http\Helpers\Helpers::isStorageFileExists($comments->image))
                                                                <div class="col-4 p-0">
                                                                    <div class="input_container">
                                                                        <p class="input_lable">Текущее фото</p>
                                                                        <img src="{{ \App\Http\Helpers\Helpers::imagePath($comments->image) }}"
                                                                             class="img-fluid"  alt="">
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            <div class="col-12 p-0">
                                                                <div class="input_container">
                                                                    <p class="input_lable">Фото</p>
                                                                    <input type="file" name="image">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 p-0">
                                                                <div class="input_container">
                                                                    <p class="input_lable">Ссылка</p>
                                                                    <input type="text" name="link" value="{{ $comments->link ?? '' }}">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="btn_container">
                                                <button class="custom_btn" type="submit">
                                                    <span>Сохранить </span>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                {{ csrf_field() }}

                            </form>


                        </div>
                    </div>
                </div>
            </main>
@endsection