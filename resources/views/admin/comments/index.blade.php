      @extends('admin.layouts.app_admin')


        @section('wrap-class')
            <div id="wrapper-products">
                @endsection

                @section('navigation')
                    @include('admin.navigation')
                @endsection

                @section('content')

                    <main>
                        <div class="section_main">
                            <div class="container-fluid p-0">
                                <div class="row m-0">
                                    <div class="col-12 p-0">
                                        <div class="header_search">
                                            <div class="title">
                                                <h1>Комментарии</h1>
                                            </div>
                                            <div class="add_btn">
                                                <a href="{{route('admin.comments.create')}}"><img
                                                            src="{{ asset('images/admin/add.svg') }}" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table_main row m-0">
                                    <div class="col-12 p-0">
                                        <div class="table_container">
                                            <table>
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <p>Имя</p>
                                                    </th>
                                                    <th>
                                                        <p>Должность</p>
                                                    </th>
                                                    <th>
                                                        <p>Комментарий</p>
                                                    </th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if ($commentsList)
                                                    @foreach ($commentsList as $item)
                                                        <tr>
                                                            <th>
                                                                <p>{{$item->name}}</p>
                                                            </th>
                                                            <th>
                                                                <p>{{$item->position}}</p>
                                                            </th>
                                                            <th>
                                                                <p>
                                                                    {{$item->comment}}
                                                                </p>
                                                            </th>


                                                            <th>
                                                                <div class="order_button">
                                                                    <img class="btn_show"
                                                                         src="{{ asset('images/admin/button_detailed_icon.png') }}"
                                                                         alt="">
                                                                    <div class="detailed_block">
                                                                        <a class="text_btn"
                                                                           href="{{route('admin.comments.update', [ 'id' => $item->id ])}}">Редактировать</a>
                                                                        <a class="text_btn"
                                                                           href="{{route('admin.comments.delete', [ 'id' => $item->id ])}}">Удалить</a>
                                                                    </div>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </main>

@endsection
