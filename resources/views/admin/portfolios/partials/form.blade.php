<div class="row m-0 product_block">
    <div class="product_block col-6 p-0">
        <div class="form_product_info">
            <div class="row m-0">
                <div class="col-12 p-0">
                    @if ($errors->any())
                        <div class="col-sm-12">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="col-6 mb-4 p-0">
                    <p class="">Главное фото</p>
                    <div class="photo_video_block">
                        <div class="images_block">
                            <div class="image_container">
                                <div class="added_icon">
                                    <img src="{{ asset('images/admin/plus-symbol.svg') }}" alt="">
                                </div>
                                <a href="#"><img src="{{ asset('images/admin/x_icon.png') }}" alt=""></a>
                                <label for="main_photo" class="@if($errors->first('main_photo')) error @endif">
                                    @isset($portfolio->main_photo)
                                        @php $portfolio->main_photo = json_decode($portfolio->main_photo) @endphp
                                        <img
                                            src=" {{ Storage::disk('upload')->url($portfolio->main_photo[1]) }}"
                                            data-img="{{ $portfolio->main_photo[0] }}"
                                            alt="">
                                        <input type="hidden" name="main_present" value="true">
                                    @endisset
                                </label>
                                <input type="file" accept=".jpg,.jpeg,.png" name="main_photo"
                                       id="main_photo"
                                       class="image-upload"/>
                                <input class="close_img" type="hidden" name="close_main_photo" value="">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-6 mb-4 p-0">
                    <p class=""> Фото для миниатюры</p>
                    <div class="photo_video_block">
                        <div class="images_block">
                            <div class="image_container">
                                <div class="added_icon">
                                    <img src="{{ asset('images/admin/plus-symbol.svg') }}" alt="">
                                </div>
                                <a href="#"><img src="{{ asset('images/admin/x_icon.png') }}" alt=""></a>
                                <label for="mini_photo" class="@if($errors->first('mini_photo')) error @endif">
                                    @isset($portfolio->mini_photo)
                                        <img src=" {{ Storage::disk('upload')->url($portfolio->mini_photo) }}"
                                             data-img="{{ $portfolio->mini_photo }}"
                                             alt="">
                                        <input type="hidden" name="mini_present" value="true">
                                    @endisset
                                </label>
                                <input type="file" accept=".jpg,.jpeg,.png" name="mini_photo"
                                       id="mini_photo"
                                       class="image-upload"/>
                                <input class="close_img" type="hidden" name="close_mini_photo" value="">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-12 p-0">
                    <div class="input_container">
                        <p class="input_lable">Заголовок</p>
                        <input name="title" placeholder="Заголовок"
                               class="@if($errors->first('title')) error @endif"
                               value="{{ $portfolio->title ?? old('title') }}">
                    </div>
                </div>
                <div class="col-12 p-0">
                    <div class="input_container">
                        <p class="input_lable">Категория</p>
                        <div class="select_container">
                            <p class="radio_select">
                                <input type="radio" name="category" value="0" @if (!isset($portfolio)) checked @elseif(!$portfolio->category) checked @endif >
                                <span>Ремонт</span>
                            </p>
                            <p class="radio_select">
                                <input type="radio" name="category" value="1" @if (!isset($portfolio))  @elseif($portfolio->category) checked @endif>
                                <span>Строительство</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 p-0">
                    <div class="input_container">
                        <p class="input_lable">Краткое описание</p>
                        <input name="desc" placeholder="Краткое описание"
                               class="@if($errors->first('desc')) error @endif"
                               value="{{ $portfolio->desc ?? old('desc') }}">
                    </div>
                </div>
                <div class="col-12 p-0">
                    <div class="input_container">
                        <p class="input_lable">Полное описание</p>
                        <textarea id="content" class="content" cols="30" rows="5" placeholder="Полное описание"
                                  class="text @if($errors->first('content')) error @endif"
                                  name="content">{{ (isset($portfolio->content)) ? $portfolio->content :  old( 'content') }}</textarea>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="col-12 pl-4 form_product_info">
        <div class="photo_video_block bottom">
            <p class="">Слайдер</p>
            {{--Array of delete images--}}
            {{--------------------------}}
            <div class="images_block">
                @if (isset($portfolio->slider))
                    {{--@php $portfolio->slider = json_decode($portfolio->slider) @endphp--}}
                    {{--@foreach($portfolio->slider as $key => $portfolio->slider)--}}
                    {{--<div>--}}
                    {{--<div class="photo_video_block">--}}
                    {{--<div class="images_block">--}}
                    {{--<div class="image_container">--}}
                    {{--<div class="added_icon">--}}
                    {{--<img src="{{ asset('images/admin/plus-symbol.svg') }}"--}}
                    {{--alt="">--}}
                    {{--</div>--}}
                    {{--<a href="#">--}}
                    {{--<img src="{{ asset('images/admin/x_icon.png') }}" alt="">--}}
                    {{--</a>--}}
                    {{--<label for="slider-{{ $key }}"--}}
                    {{--class="@if($errors->first('slider_photo')) error @endif">--}}
                    {{--<img style="width: 100%"--}}
                    {{--src="{{ Storage::disk('upload')->url($portfolio->slider) }}"--}}
                    {{--alt=""--}}
                    {{--data-img="{{ $portfolio->slider }}">--}}
                    {{--<input type="hidden" name="slider-{{ $key }}" value="true">--}}
                    {{--</label>--}}
                    {{--<input type="file" accept=".jpg,.jpeg,.png"--}}
                    {{--name="slider_photo[]"--}}
                    {{--id="slider-{{ $key }}"--}}
                    {{--class="image-upload"/>--}}
                    {{--<input class="close_img" type="hidden" name="del_slider[]"--}}
                    {{--value="">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@endforeach--}}
                    @for($i = 0; $i < 7; $i++)
                        <div>
                            {{--@php if(array_key_exists($i, $portfolio->slider)) dump($portfolio->slider[$i])  @endphp--}}
                            <div class="photo_video_block">
                                <div class="images_block">
                                    <div class="image_container">

                                        <div class="added_icon">
                                            <img src="{{ asset('images/admin/plus-symbol.svg') }}"
                                                 alt="">
                                        </div>
                                        <a href="#">
                                            <img src="{{ asset('images/admin/x_icon.png') }}" alt="">
                                        </a>
                                        <label for="slider-{{ $i }}">
                                            @if (array_has((array)$portfolio->slider, $i))
                                                <img style="width: 100%"
                                                     src="{{ Storage::disk('upload')->url($portfolio->slider[$i]) }}"
                                                     alt=""
                                                     data-img="{{ $portfolio->slider[$i] }}">
                                                <input type="hidden" name="slider-{{ $i }}" value="true">
                                            @endisset
                                        </label>
                                        <input type="file" accept=".jpg,.jpeg,.png"
                                               name="slider_photo[]"
                                               id="slider-{{ $i }}"
                                               class="image-upload"/>


                                        <input class="close_img" type="hidden" name="del_slider[]" value="">

                                        <input type="text" name="sort[]" class="position-static" placeholder="порядок соритровки">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                @else
                    @for($i = 0; $i < 7; $i++)
                        <div>
                            <div class="photo_video_block">
                                <div class="images_block">
                                    <div class="image_container">
                                        <div class="added_icon">
                                            <img src="{{ asset('images/admin/plus-symbol.svg') }}"
                                                 alt="">
                                        </div>
                                        <a href="#">
                                            <img src="{{ asset('images/admin/x_icon.png') }}" alt="">
                                        </a>
                                        <label for="slider-{{ $i }}"></label>
                                        <input type="file" accept=".jpg,.jpeg,.png"
                                               name="slider_photo[]"
                                               id="slider-{{ $i }}"
                                               class="image-upload"/>
                                        <input class="close_img" type="hidden" name="del_slider[]" value="">
                                        {{--<input class="close_img" type="hidden" name="close_slider_photo[]" value="">--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor

                    {{----}}
                    {{--<div>--}}
                    {{--<div class="photo_video_block">--}}
                    {{--<div class="images_block">--}}
                    {{--<div class="image_container">--}}
                    {{--<div class="added_icon">--}}
                    {{--<img src="{{ asset('images/admin/plus-symbol.svg') }}"--}}
                    {{--alt="">--}}
                    {{--</div>--}}
                    {{--<a href="#">--}}
                    {{--<img src="{{ asset('images/admin/x_icon.png') }}" alt="">--}}
                    {{--</a>--}}
                    {{--<label for="slider-1"></label>--}}
                    {{--<input type="file" accept=".jpg,.jpeg,.png"--}}
                    {{--name="slider_photo[]"--}}
                    {{--id="slider-1"--}}
                    {{--class="image-upload"/>--}}
                    {{--<input class="close_img" type="hidden" name="del_slider[]" value="">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{----}}
                    {{--<div>--}}
                    {{--<div class="photo_video_block">--}}
                    {{--<div class="images_block">--}}
                    {{--<div class="image_container">--}}
                    {{--<div class="added_icon">--}}
                    {{--<img src="{{ asset('images/admin/plus-symbol.svg') }}"--}}
                    {{--alt="">--}}
                    {{--</div>--}}
                    {{--<a href="#">--}}
                    {{--<img src="{{ asset('images/admin/x_icon.png') }}" alt="">--}}
                    {{--</a>--}}
                    {{--<label for="slider-2"></label>--}}
                    {{--<input type="file" accept=".jpg,.jpeg,.png"--}}
                    {{--name="slider_photo[]"--}}
                    {{--id="slider-2"--}}
                    {{--class="image-upload"/>--}}
                    {{--<input class="close_img" type="hidden" name="del_slider[]" value="">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                @endif
            </div>
        </div>
    </div>

    <div class="btn_container pl-4">
        <button class="custom_btn" type="submit">
                <span>@if (Route::current()->getName() == 'admin.portfolio.edit') Изменить портфолио @else Создать
                    портфолио@endif</span>
        </button>
    </div>

</div>

