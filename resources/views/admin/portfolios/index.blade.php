@extends('admin.layouts.app_admin')


@section('wrap-class')
    <div id="wrapper-products">
        @endsection

        @section('navigation')
            @include('admin.navigation')
        @endsection

        @section('content')

            <main>
                <div class="section_main">
                    <div class="container-fluid p-0">
                        <div class="row m-0">
                            <div class="col-12 p-0">
                                <div class="header_search">
                                    <div class="title">
                                        <h1>Портфолио</h1>
                                    </div>
                                    <div class="add_btn">
                                        <a href="{{route('admin.portfolio.create')}}"><img
                                                src="{{ asset('images/admin/add.svg') }}" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table_main row m-0">
                            <div class="col-12 p-0">
                                <div class="table_container">
                                    <table>
                                        <thead>
                                        <tr>
                                            <th>
                                                <p>Заголовок</p>
                                            </th>
                                            <th>
                                                <p>Краткое описание</p>
                                            </th>
                                            <th>
                                                <p>Категория</p>
                                            </th>
                                            <th>
                                                <p>Статус</p>
                                            </th>
                                            <th>
                                                <p>Позиция</p>
                                            </th>
                                            <th>
                                                <p>Действие</p>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @forelse($portfolios as $portfolio)
                                            @php $portfolio->main_photo = json_decode($portfolio->main_photo);
                                            @endphp
                                            <tr>
                                                <th>
                                                    <div class="product_container">
                                                        <div class="product_image">
                                                            @isset($portfolio->main_photo[1])
                                                                <img
                                                                    src=" {{ Storage::disk('upload')->url($portfolio->main_photo[1]) }}"
                                                                    alt="">
                                                            @endisset
                                                        </div>
                                                        <div class="product_name">
                                                            <p>{{ $portfolio->title ?? '' }}</p>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th>
                                                    <p>{{ $portfolio->desc ?? '' }}</p>
                                                </th>
                                                <th>
                                                    <p>
                                                        @if($portfolio->category) Строительство @else Ремонт @endif
                                                    </p>
                                                </th>

                                                <th>
                                                    <p>@if ( isset($portfolio->status) && ($portfolio->status))
                                                            Активна @else Скрыта @endif</p>
                                                </th>
                                                <th>
                                                    {{--<p>{{ $portfolio->position ?? '' }}</p>--}}
                                                    <select class="portfolio_position" data-action="{{ route('admin.portfolio.position', $portfolio->id)  }}"
                                                            name="position">
                                                        @foreach($portfolios as $key => $item)
                                                            <option value="{{$item->position}}" @if($item->position == $portfolio->position) selected @endif> {{$key + 1}}</option>
                                                        @endforeach

                                                    </select>
                                                </th>
                                                <th>
                                                    <div class="order_button">
                                                        <img class="btn_show"
                                                             src="{{ asset('images/admin/button_detailed_icon.png') }}"
                                                             alt="">
                                                        <div class="detailed_block">
                                                            <a class="text_btn"
                                                               href="{{route('admin.portfolio.edit', $portfolio)}}">Редактировать</a>
                                                            <form class="text_btn delete"
                                                                  action="{{route('admin.portfolio.destroy', $portfolio)}}"
                                                                  method="post">
                                                                <input type="hidden" name="_method" value="delete">
                                                                {{ csrf_field() }}
                                                                <button type="submit" class="text_btn"
                                                                        href="{{route('admin.portfolio.destroy', $portfolio)}}">
                                                                    Удалить
                                                                </button>
                                                            </form>
                                                            <a class="text_btn"
                                                               href="{{route('admin.portfolio.hide', $portfolio->id)}}">@if ($portfolio->status)
                                                                    Скрыть @else  Показать @endif</a>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td>
                                                    <h3 style="text-alight:center">Данные отсутствуют</h3>
                                                </td>
                                            </tr>
                                        @endforelse

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </main>

@endsection
