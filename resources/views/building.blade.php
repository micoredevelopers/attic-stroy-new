@extends('layouts.app')
@section('title', 'Строительство домов Одесса - цена, стоимость услуг, отзывы | Attic-stroy')
@section('desc', 'Услуги строительства зданий, домов и коттеджей в Одессе и Одесской области от компании【ATTIC-STROY】Звоните ✆ +38 (063) 153-53-33')
@section('content')
    <main id="wrapper_building">

        <section class="build_info_section">
            <div class="container">
            <div itemscope="" itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" style="margin-top:0;margin-bottom:30px">
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a rel="nofollow" itemprop="item" title="Строительная компания в Одессе" href="{{ route('index') }}">
          <span itemprop="name">Строительная компания в Одессе</span>
          <meta itemprop="position" content="1">
       </a>
   </span> > 
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a itemprop="item" title=" Строительство домов" href="{{ route('building') }}">
          <span itemprop="name">Строительство домов</span>
          <meta itemprop="position" content="2">
       </a>
   </span>
</div>
                <div class="row m-0 top_row">
                
                    <div class="col-12 p-0 d-block d-md-none wow animated fadeInLeft" data-wow-duration="0.8s"
                         data-wow-delay="0s">
                        <div class="block_info top">
                            <div class="image_container">
                                <img src="{{ asset('images/building/first-image.jpg') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 p-0 d-block d-md-none wow animated fadeInLeft" data-wow-duration="0.8s"
                         data-wow-delay="0.2s">
                        <div class="block_info top">
                            <div class="text_info">
                            <h1 class="title">Строительство домов в Одессе</h1>
                                <h2 class="title">Каждый проект начинается с идеи</h2>
                                <p class="text">
                                Еще до начала строительства и разработки проектной документации мы создаем с клиентом проект будущего дома. Данный проект состоит из  планов, фасадов, разрезов и визуализации, позволяющей увидеть дом в 3D модели. Этого уже достаточно для получения разрешения на строительство. <br>
                                <br>
                                Мы подробно описываем все детали: подбираем материалы, рассчитываем все конструкции, размеры и элементы будущего дома. Рабочий проект выполняется индивидуально под потребности заказчика.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-5 p-0 d-none d-md-block wow animated fadeInRight" data-wow-duration="0.8s"
                         data-wow-delay="0s">
                        <div class="block_info top">
                            <div class="text_info">
                            <h2 class="title">Строительство домов в Одессе</h2>
                                <h2 class="title">Каждый проект начинается с идеи</h2>
                                <p class="text">
                                Еще до начала строительства и разработки проектной документации мы создаем с клиентом проект будущего дома. Данный проект состоит из  планов, фасадов, разрезов и визуализации, позволяющей увидеть дом в 3D модели. Этого уже достаточно для получения разрешения на строительство. <br>
                                <br>
                                Мы подробно описываем все детали: подбираем материалы, рассчитываем все конструкции, размеры и элементы будущего дома. Рабочий проект выполняется индивидуально под потребности заказчика.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-7 p-0 d-none d-md-block wow animated fadeInLeft" data-wow-duration="0.8s"
                         data-wow-delay="0.2s">
                        <div class="block_info top">
                            <div class="image_container">
                                <img src="{{ asset('images/building/first-image.jpg') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-0 bottom_row">
                    <div class="col-xl-6 col-md-7 col-12 p-0 wow animated fadeInRight" data-wow-duration="0.8s"
                         data-wow-delay="0s">
                        <div class="block_info bottom">
                            <div class="image_container">
                                <img src="{{ asset('images/building/second-image.jpg') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-5 col-12 p-0 wow animated fadeInLeft" data-wow-duration="0.8s"
                         data-wow-delay="0.2s">
                        <div class="block_info bottom">
                            <div class="text_info">
                                <h2 class="title">Технологии строительства</h2>
                                <p class="text">
                                В работе мы используем только современные и проверенные десятилетиями технологии строительства. Мы делаем все, чтобы гарантировать долгосрочную эксплуатацию вашего загородного дома. Для каждого проекта наши специалисты индивидуально подбирают материалы и технические решения. Мы всегда учитываем климатические и геологические факторы, выбирая технологии с повышенным запасом прочности. Поэтому на все наши дома гарантия по договору от 10 лет.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="title_cont">
                    <h2 class="title">Стоимость строительства от 325 у.е.</h2>
                </div>
            </div>
        </section>
        <section class="building_slide_section">
            <div class="container">
                <div class="tab_row wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">
                    <div data-id="foundation" class="tab_item active">Фундамент</div>
                    <span>|</span>
                    <div data-id="houses" class="tab_item center">Дома</div>
                    <span>|</span>
                    <div data-id="landscape" class="tab_item">Ландшафт</div>
                </div>
                <div class="sliders_cont">
                    <div id="foundation" class="building_slider found active">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="row m-0">
                                    <div class="col-xl-6 col-md-7 col-12 p-0">
                                        <div class="image_container">
                                            <img src="{{ asset('images/building/fund.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-5 col-12 p-0">
                                        <div class="text_info wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.45s">
                                            <h3 class="title">Фундамент</h3>
                                            <p class="text">
                                                Надежный оплот будущего дома, который должен легко справляться с физическими нагрузками и перепадами температур на протяжении долгих десятилетий. Перед созданием фундамента наши специалисты проводят инженерно-геологические изыскания чтобы определить состав и структуру грунта. На основе этих данных мы определяем самую эффективную и оптимальную технологию строительства фундамента.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="row m-0">
                                    <div class="col-xl-6 col-md-7 col-12 p-0">
                                        <div class="image_container">
                                            <img src="{{ asset('images/building/fund1.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-5 col-12 p-0">
                                        <div class="text_info wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.45s">
                                            <h3 class="title">Фундамент</h3>
                                            <p class="text">
                                                Надежный оплот будущего дома, который должен легко справляться с физическими нагрузками и перепадами температур на протяжении долгих десятилетий. Перед созданием фундамента наши специалисты проводят инженерно-геологические изыскания чтобы определить состав и структуру грунта. На основе этих данных мы определяем самую эффективную и оптимальную технологию строительства фундамента.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="row m-0">
                                    <div class="col-xl-6 col-md-7 col-12 p-0">
                                        <div class="image_container">
                                            <img src="{{ asset('images/building/fund2.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-5 col-12 p-0">
                                        <div class="text_info wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.45s">
                                            <h3 class="title">Фундамент</h3>
                                            <p class="text">
                                                Надежный оплот будущего дома, который должен легко справляться с физическими нагрузками и перепадами температур на протяжении долгих десятилетий. Перед созданием фундамента наши специалисты проводят инженерно-геологические изыскания чтобы определить состав и структуру грунта. На основе этих данных мы определяем самую эффективную и оптимальную технологию строительства фундамента.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="row m-0">
                                    <div class="col-xl-6 col-md-7 col-12 p-0">
                                        <div class="image_container">
                                            <img src="{{ asset('images/building/fund3.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-5 col-12 p-0">
                                        <div class="text_info wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.45s">
                                            <h3 class="title">Фундамент</h3>
                                            <p class="text">
                                                Надежный оплот будущего дома, который должен легко справляться с физическими нагрузками и перепадами температур на протяжении долгих десятилетий. Перед созданием фундамента наши специалисты проводят инженерно-геологические изыскания чтобы определить состав и структуру грунта. На основе этих данных мы определяем самую эффективную и оптимальную технологию строительства фундамента.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s"></div>
                        <div class="swiper-button-prev wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <i class="arrow left"></i>
                        </div>
                        <div class="swiper-button-next wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <i class="arrow right"></i>
                        </div>
                        <div class="circle_container wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <div class="slide-circle"></div>
                        </div>
                    </div>
                    <div id="houses" class="building_slider houses">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="row m-0">
                                    <div class="col-xl-6 col-md-7 col-12 p-0">
                                        <div class="image_container">
                                            <img src="{{ asset('images/building/home-3.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-5 col-12 p-0">
                                        <div class="text_info wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.45s">
                                            <h3 class="title">Дом из кирпича</h3>
                                            <p class="text">
                                            Строительство дома из кирпича - это классический вариант, который пользуется большой популярностью в первую очередь из-за экологичности, а также огнеупорных и теплоизоляционных характеристик самого кирпича. Единственный особенность строительства дома из кирпича - это высокая стоимость материалов и более долгий срок работы.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="row m-0">
                                    <div class="col-xl-6 col-md-7 col-12 p-0">
                                        <div class="image_container">
                                            <img src="{{ asset('images/building/home-gazobeton.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-5 col-12 p-0">
                                        <div class="text_info">
                                            <h3 class="title">Дом из газобетона</h3>
                                            <p class="text">
                                            Газобетон обладает отличными теплоизоляционными свойствами и доступной стоимостью. С помощью газобетонных блоков процесс строительства идет гораздо быстрее, чем при использовании того же кирпича. Главная особенность этого материала: высокие требования к применяемым технологиям строительства из-за хрупкости блоков.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s"></div>
                        <div class="swiper-button-prev wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <i class="arrow left"></i>
                        </div>
                        <div class="swiper-button-next wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <i class="arrow right"></i>
                        </div>
                        <div class="circle_container wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <div class="slide-circle"></div>
                        </div>
                    </div>
                    <div id="landscape" class="building_slider landscape">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="row m-0">
                                    <div class="col-xl-6 col-md-7 col-12 p-0">
                                        <div class="image_container">
                                            <img src="{{ asset('images/building/landscape-1.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-5 col-12 p-0">
                                        <div class="text_info">
                                            <h3 class="title">Ландшафт</h3>
                                            <p class="text">
                                            По приезду домой вас будет встречать обычный двор или ваш собственный сад? Вот на этот вопрос отвечает ландшафтный дизайн. Мы помогаем определить будущий стиль участка, разрабатываем план по озеленению и схему автоматического полива, подбираем газон и прокладываем декоративные дорожки.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="row m-0">
                                    <div class="col-xl-6 col-md-7 col-12 p-0">
                                        <div class="image_container">
                                            <img src="{{ asset('images/building/landscape-2.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-5 col-12 p-0">
                                        <div class="text_info">
                                            <h3 class="title">Ландшафт</h3>
                                            <p class="text">
                                            Наш опыт работы в ландшафтном дизайне позволяет создавать индивидуальные проекты для каждого заказчика. Мы поможем собрать такую комбинацию дорожек, фонарей, цветов, кустов и деревьев, которая станет сердцем вашего двора.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-no-swiping">
                                <div class="row m-0">
                                    <div class="col-xl-6 col-md-7 col-12 p-0">
                                        <div class="image_container">
                                            <img src="{{ asset('images/building/landscape-3.jpg') }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-5 col-12 p-0">
                                        <div class="text_info">
                                            <h3 class="title">Ландшафт</h3>
                                            <p class="text">
                                            Для каждого проекта мы предлагаем уникальные и креативные решения исходя из пожеланий клиента. Также мы берем на себя решение всех вопросов связанных с закупкой и посадкой растений. И обязательно расскажем о простом и правильном уходе за вашей зеленой зоной.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s"></div>
                        <div class="swiper-button-prev wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <i class="arrow left"></i>
                        </div>
                        <div class="swiper-button-next wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <i class="arrow right"></i>
                        </div>
                        <div class="circle_container wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <div class="slide-circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section> 

        <section class="slide_section">
            <div class="container">
                <div class="title_cont wow animated fadeInUp" data-wow-duration="0.6s">
                    <h2 class="title">При заказе строительства проект Вашего дома в подарок</h2>
                </div>
                <div class="slide_cont">
                    <div class="gallery_slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/1.jpeg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/2.jpg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/3.jpg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/4.jpg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/5.jpg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/6.jpg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/7.jpg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/8.jpg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/9.jpg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/10.jpeg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="slide_info">
                                    <div class="image_container">
                                        <img src="{{ asset('images/building/11.jpeg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.45s"></div>
                        <div class="swiper-button-prev wow animated fadeInLeft" data-wow-duration="0.5s" data-wow-delay="0.45s">
                            <img src="{{ asset('images/about/slide-arrow.svg') }}" alt="">
                        </div>
                        <div class="swiper-button-next wow animated fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.45s">
                            <img src="{{ asset('images/about/slide-arrow.svg') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="review_slide_section">
            <div class="review_slider">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="row m-0">
                            <div class="col-12 p-0 d-block d-md-none">
                                <div class="review_user_info">
                                    <div class="img_cont">
                                        <img src="{{ asset('images/repair/employee-img.png') }}" alt="">
                                    </div>
                                    <h3 class="title">Фамилия, имя</h3>
                                    <h5 class="sub_title">Анатолиевна Анна</h5>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-9 col-12 p-0">
                                <div class="bottom_info">
                                    <p class="text">Изысканная бессарабская и средиземноморская кухни, приятный дизайн
                                        из натуральных материалов и исконно бессарабские мотивы в интерьере с первого
                                        взгляда погружают в неповторимую атмосферу.</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-3 p-0 d-none d-md-block">
                                <div class="review_user_info">
                                    <div class="img_cont">
                                        <img src="{{ asset('images/repair/employee-img.png') }}" alt="">
                                    </div>
                                    <h3 class="title">Фамилия, имя</h3>
                                    <h5 class="sub_title">Анатолиевна Анна</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination">
                </div>
                <div class="swiper-button-prev">
                    <i class="arrow left"></i>
                </div>
                <div class="swiper-button-next">
                    <i class="arrow right"></i>
                </div>
                <div class="slide-circle"></div>
            </div>
        </section>
        <section class="advantages_section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-12 wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0s">
                        <div class="advantage_block">
                            <div class="image_container">
                                <img src="{{ asset('images/building/attic-icons_time.svg') }}" alt="">
                            </div>
                            <div class="text_info">
                                <h4 class="title">Скорость</h4>
                                <p class="text">Выполнение работ точно в срок по договору или раньше.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-12 wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.15s">
                        <div class="advantage_block">
                            <div class="image_container">
                                <img src="{{ asset('images/building/attic-icons_quality.svg') }}" alt="">
                            </div>
                            <div class="text_info">
                                <h4 class="title">Качество</h4>
                                <p class="text">Гарантия на все выполненные работы, прописанная в договоре.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-12 wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">
                        <div class="advantage_block">
                            <div class="image_container">
                                <img src="{{ asset('images/building/attic-icons_warranty.svg') }}" alt="">
                            </div>
                            <div class="text_info">
                                <h4 class="title">Честность</h4>
                                <p class="text">Прозрачные и понятные финансовые сметы, детальная отчетность на каждом этапе.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="object_calc">
            <div class="container">
                <div class="top_info">
                    <div class="image_container">
                        <img src="{{ asset('images/building/calc-image.jpg') }}" alt="">
                    </div>
                    <div class="text_info wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.1s">
                        <h3 class="title">Получите бесплатный расчет</h3>
                        <p class="text">
                            Консультация со специалистом. Помощь в выборе технологий и материалов. Личная встреча.
                        </p>
                    </div>
                </div>

                <form class="form_cont contact_send" action="{{ route('building.send') }}" method="post">
                    <fieldset class="material wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.2s">
                        <input class="input_cont" type="text" required name="name">
                        <hr>
                        <label>Как Вас зовут?</label>
                    </fieldset>
                    <fieldset class="material wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                        <input class="input_cont number" type="text" required name="phone">
                        <hr>
                        <label>Ваш номер телефона?</label>
                    </fieldset>
                    <div class="custom_btn black wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.4s">
                        <button class="my_btn black" type="submit">
                            <span>Отправить</span>
                            <img src="{{ asset('images/contacts/submit-btn-arrow.svg') }}" alt="">
                        </button>
                    </div>
                    {{ csrf_field() }}
                    <input type="hidden" name="type" value="Строительство">
                </form>
                <!-- <div class="custom_btn wow animated fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                    <a class="my_btn portf" href="{{ route('portfolio') }}"><span>Портфолио</span></a>
                </div> -->
            </div>
        </section>
        <section class="about_bottom">
            <div class="container">
                <div class="section_title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0s" style="display:none">
                    <h2 class="title">ПОДРОБНЕЕ</h2>
                </div>
                <div class="text_block wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.15s">
                    <div class="text"><h2 style="text-align: center;">Строительство домов в Одессе от компании Attic Stroy</h2>
<p>Более 10 лет занимаясь строительством домов под ключ в Одессе, компания Attic Stroy приобрела большой опыт работы на рынке жилой и коммерческой недвижимости. Мы помогаем нашим клиентам воплотить свою мечту о жизни на загородном участке или в черте города с максимальным комфортом.</p>
<h2 style="text-align: center;">Как выполняются работы?</h2>
<p>Перед составлением проекта нам нужно узнать обо всех желаниях клиента. Должны быть учтены следующие факторы:</p>
<ul>
<li>Геологические особенности участка. От них зависит выбор типа фундамента и технологии строительства дома.</li>
<li>Бюджет &ndash; при составлении сметы мы можем сократить расходы на материалы до 20%, поскольку мы сотрудничаем с проверенными партнерами.</li>
<li>Стилевые предпочтения заказчика &ndash; создание комфортного жилья требует </li>
</ul>
<p>Закажите строительство дома в Одессе в компании Attic Stroy и вы получите бесплатный расчет стоимости работ.</p>
<h3 style="text-align: center;">Подготовка к строительству</h3>
<p>На этапе закупки мы консультируем клиентов по выбору наиболее качественных стройматериалов. Это позволяет нам гарантировать длительный срок службы инженерных систем и конструкций.</p>
<p>Полная смета составляется до начала строительства, поэтому вам не придется планировать бюджет исходя из возможных дополнительных трат.</p>
<p>Последовательность работ:</p>
<ul>
<li>выяснение предпочтений клиента;</li>
<li>проведение инженерно-геологических изысканий на участке;</li>
<li>подготовка проектной документации;</li>
<li>расчет стоимости;</li>
<li>начало строительства;</li>
<li>инженерные работы;</li>
<li>чистовая и финишная отделка;</li>
<li>сдача объекта в эксплуатацию.</li>
</ul>
<p>В зависимости от целей клиента, мы создаем индивидуальный или выбираем типовой проект. После постройки дома и вывоза мусора выполняется обустройство территории.</p>
<h3 style="text-align: center;">Что мы предлагаем?</h3>
<p>В список наших объектов входят:</p>
<ul>
<li>частные дома;</li>
<li>коттеджи;</li>
<li>таунхаусы;</li>
<li>гостиницы.</li>
</ul>
<p>Занимаясь строительством частных домов в Одессе, мы приобрели бесценный опыт работы с клиентами. Мы проникаемся интересами каждого заказчика и можем гарантировать высокий сервис &ndash; предоставление информации по запросу, регулярность отчетов, оформление документов.</p>
<h3 style="text-align: center;">Наши преимущества</h3>
<p>Благодаря сотрудничеству с проверенными партнерами мы можем сократить время на поиск материалов. Чтобы вам не пришлось заказывать услуги транспортных компаний, мы занимаемся доставкой.</p>
<p>Наши преимущества:</p>
<ul>
<li>бесплатный предварительный расчет стоимости строительства;</li>
<li>контроль текущих расходов в строгом соответствии со сметой;</li>
<li>возможность воплощения нестандартных решений;</li>
<li>предоставление готового проекта в подарок при заказе строительства домов в Одессе.</li>
</ul>
<p>Сроки строительства зависят от особенностей проекта, предпочтений заказчика и запланированного объема работ.</p>
<h3 style="text-align: center;">Гарантии</h3>
<p>Поскольку мы давно занимаемся возведением надежных и долговечных зданий, у нас есть большой каталог архитектурных решений. На строительство домов по договору мы даем гарантию 10 лет.</p>
<h2 style="text-align: center;">Заказать строительство дома в Одессе</h2>
<p>Компания Attic Stroy предлагает вам построить дом своей мечты прямо сейчас. Закажите личную встречу с нашим специалистом, чтобы рассчитать примерные сроки работ. Связаться с нами можно по телефону и электронной почте. Оставьте заявку в форме обратной связи, и мы напишем вам в ближайшее время.</p>
                    </div>
                </div>
                <div class="custom_btn show_more black wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">
                    <div class="my_btn"><span>Подробнее</span></div>
                </div>
            </div>
        </section>
    </main>
@endsection

