@extends('layouts.app')
@section('title', 'Строительная компания Одесса - цена, стоимость услуг, отзывы | Attic-stroy')
@section('desc', 'Строительная компания【ATTIC-STROY】предлагает ►ремонт квартир в Одессе и строительство домов недорого◀ Звоните ✆ +38 (063) 153-53-33')
@section('content')

    <main id="wrapper_main">
        <div class="main_block_container mobile d-block d-xl-none">
            <div class="main_block bottom">
                <div class="block_bg">
                    <img src="{{ asset('images/main/main-repair.jpg') }}" alt="" class="bg">
                </div>
                <div class="bg_overlay"></div>
                <div class="block_info">
                    <div class="title_cont">
                        <h1 class="title">Ремонт квартир</h1>
                    </div>
                    <div class="text_cont">
                        <p class="text">
                            Реализовываем любые дизайнерские идеи и самые смелые интерьерные решения. 
                            Пятилетняя гарантия на все виды услуг. Работа по официальном договору с утвержденной сметой.
                        </p>
                    </div>
                    <div class="custom_btn">
                        <a href="{{ route('repair') }}" class="my_btn">
                            <span>Подробнее</span>
                            <img src="{{ asset('images/main/portfolio_arrow.svg') }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="main_block top">
                <div class="block_bg">
                    <img src="{{ asset('images/main/main-building.jpg') }}" alt="" class="bg">
                </div>
                <div class="bg_overlay"></div>
                <div class="block_info">
                    <div class="title_cont">
                        <h2 class="title">Строительство домов</h2>
                    </div>
                    <div class="text_cont">
                        <p class="text">
                        Проектирование и строительство домов,  коттеджей из кирпича, газобетона и керамических блоков. 
                        Реализация проектов под ключ, начиная от фундамента и заканчивая ландшафтным дизайном. При заказе строительства проект дома в подарок.
                        </p>
                    </div>
                    <div class="custom_btn">
                        <a href="{{ route('building') }}" class="my_btn">
                            <span>Подробнее</span>
                            <img src="{{ asset('images/main/portfolio_arrow.svg') }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_block_container desktop d-none d-xl-flex">
            <div class="main_block left">
                <div class="block_bg">
                    <img src="{{ asset('images/main/main-repair.jpg') }}" alt="" class="bg">
                </div>
                <div class="bg_overlay"></div>
                <div class="block_info">
                    <div class="title_cont small">
                        <h1 class="title">Ремонт квартир</h1>
                    </div>
                    <div class="title_cont big">
                        <h1 class="title">Ремонт квартир</h1>
                    </div>
                    <div class="text_cont">
                        <p class="text">
                        Реализовываем любые дизайнерские идеи и самые смелые интерьерные решения. Пятилетняя гарантия на все виды услуг. Работа по официальном договору с утвержденной сметой.
                        </p>
                    </div>
                    <div class="custom_btn">
                        <a href="{{ route('repair') }}" class="my_btn"><span>Подробнее</span></a>
                    </div>
                </div>
            </div>
            <div class="main_block right">
                <div class="skew_line">
                    <img src="{{ asset('images/main/main-building.jpg') }}" alt="" class="bg">
                    <div class="bg_overlay"></div>
                </div>
                <div class="block_info">
                    <div class="title_cont small">
                        <h2 class="title">Строительство домов</h2>
                    </div>
                    <div class="title_cont big">
                        <h2 class="title">Строительство домов</h2>
                    </div>
                    <div class="text_cont">
                        <p class="text">
                        Проектирование и строительство домов,  коттеджей из кирпича, газобетона и керамических блоков. Реализация проектов под ключ, начиная от фундамента и заканчивая ландшафтным дизайном. При заказе строительства проект дома в подарок.
                        </p>
                    </div>
                    <div class="custom_btn">
                        <a href="{{ route('building') }}" class="my_btn"><span>Подробнее</span></a>
                    </div>
                </div>
            </div>
        </div>

    </main>
@endsection

@section('footer')
@stop




