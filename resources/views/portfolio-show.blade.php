@extends('layouts.app')
@section('title', $portfolio->title)
@section('content')
    @php
        $portfolio->main_photo = json_decode($portfolio->main_photo);
    @endphp
    <main id="wrapper_selected_portfolio">
        <div class="container">
        <div itemscope="" itemtype="http://schema.org/BreadcrumbList" id="breadcrumbs" style="margin-top:0;margin-bottom:30px;">
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a rel="nofollow" itemprop="item" title="Строительная компания в Одессе" href="{{ route('index') }}">
          <span itemprop="name">Строительная компания в Одессе</span>
          <meta itemprop="position" content="1">
       </a>
   </span> > 
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a itemprop="item" title="Портфолио" href="{{ route('portfolio') }}">
          <span itemprop="name">Портфолио</span>
          <meta itemprop="position" content="2">
       </a>
   </span> >
   <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
       <a itemprop="item" title="{{ $portfolio->title ?? '' }}" href="{{ $portfolio->url ?? '' }}">
          <span itemprop="name">{{ $portfolio->title ?? '' }}</span>
          <meta itemprop="position" content="3">
       </a>
   </span>
</div>
            <div class="portfolio_info">
                <div class="row m-0">
                    <div class="col-12 p-0 d-block d-xl-none">
                        <div class="image_container">
                            @isset($portfolio->main_photo)
                                <img src=" {{ Storage::disk('upload')->url($portfolio->main_photo[0]) }}" alt="">
                            @endisset
                        </div>
                    </div>
                    <div class="col-xl-6 col-12 p-0 left_block">
                        <div class="text_info">
                            <h1 class="title wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">{{ $portfolio->title ?? '' }}</h1>
                            <div class="text wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.35s">
                                {!!  $portfolio->content ?? '' !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 p-0 d-none d-xl-block">
                        <div class="image_container">
                            @isset($portfolio->main_photo)
                                <img src=" {{ Storage::disk('upload')->url($portfolio->main_photo[0]) }}" alt="">
                            @endisset
                        </div>
                    </div>
                </div>
            </div>
            <div class="portfolio_slider">
                <!-- Slider main container -->
                <div class="swiper-container selected-portfolio-slider">
                    <div class="swiper-wrapper">
                        @isset($portfolio->slider)
                            @foreach($portfolio->slider as $slider)
                                <div class="swiper-slide">
                                    <div class="image_container">
                                        <img src="{{ Storage::disk('upload')->url($slider) }}" alt="">
                                    </div>
                                </div>
                            @endforeach
                        @endisset
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.45s"></div>

                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev wow animated fadeInLeft" data-wow-duration="0.6s" data-wow-delay="0.25s">
                        <img src="{{ asset('images/selected-portfolio/selected-portfolio-arrow-left.png') }}" alt="">
                    </div>
                    <div class="swiper-button-next wow animated fadeInRight" data-wow-duration="0.6s" data-wow-delay="0.25s">
                        <img src="{{ asset('images/selected-portfolio/selected-portfolio-arrow-right.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="feedback_block">
                <div class="row m-0">
                    <div class="col-md-6 col-12 p-0">
                        <div class="image_container">
                            @isset($portfolio->mini_photo)
                                <img src="{{ Storage::disk('upload')->url($portfolio->mini_photo) }}" alt="">
                            @endisset
                        </div>
                    </div>
                    <div class="col-md-6 col-12 p-0">
                        <div class="text_info">
                            <h1 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.15s">Хотите такой же ремонт в своей квартире?</h1>
                            <p class="text wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">Оставьте заявку на консультацию и мы бесплатно подготовим для вас все расчеты, назначим личную встречу в Одессе и поможем определиться с интерьером</p>
                        </div>
                        <div class="custom_btn black wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.45s">
                            <div class="my_btn" data-toggle="modal" data-target="#feedback_modal"><span>Оставить заявку</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="works_navigation">
                <div class="row m-0">
                    @isset($portfolio_prev)
                        <div class="col-sm-6 col-12 p-0">
                            <a href="{{ route('portfolio.show', $portfolio_prev->id) }}">
                                <div class="work_block prev">
                                    <div class="link_btn wow animated fadeInLeft" data-wow-duration="0.65s" data-wow-delay="0.7s">
                                        <img src="{{ asset('images/selected-portfolio/portfolio_arrow.svg') }}" alt="">
                                        <span>Предыдущая работа</span>
                                    </div>
                                    <div class="image_container">
                                        @isset($portfolio_prev->mini_photo)
                                            <img src="{{ Storage::disk('upload')->url($portfolio_prev->mini_photo) }}" alt="">
                                        @endisset
                                    </div>
                                    <div class="work_info">
                                        <h1 class="title wow animated fadeInUp" data-wow-duration="0.65s" data-wow-delay="1s">{{ $portfolio_prev->title ?? '' }}</h1>
                                        <div class="text wow animated fadeInUp" data-wow-duration="0.65s" data-wow-delay="1.15s">{!! $portfolio_prev->content ?? '' !!} </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endisset
                    @isset($portfolio_next)
                        <div class="col-sm-6 col-12 p-0">
                            <a href="{{ route('portfolio.show', $portfolio_next->id) }}">
                                <div class="work_block next">
                                    <div class="link_btn wow animated fadeInRight" data-wow-duration="0.65s" data-wow-delay="0.7s">
                                        <span>Следующая работа</span>
                                        <img src="{{ asset('images/selected-portfolio/portfolio_arrow.svg') }}" alt="">
                                    </div>
                                    <div class="image_container">
                                        @isset($portfolio_next->mini_photo)
                                            <img src="{{ Storage::disk('upload')->url($portfolio_next->mini_photo) }}" alt="">
                                        @endisset
                                    </div>
                                    <div class="work_info">
                                        <h1 class="title wow animated fadeInUp" data-wow-duration="0.65s" data-wow-delay="1.2s">{{ $portfolio_next->title ?? '' }}</h1>
                                        <div class="text wow animated fadeInUp" data-wow-duration="0.65s" data-wow-delay="1.35s">{!! $portfolio_next->content ?? '' !!} </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endisset

                </div>
                <div class="custom_btn black wow animated fadeInUp" data-wow-duration="0.65s" data-wow-delay="1.5s">
                    <a href="{{ route('portfolio') }}" class="my_btn"><span>Все работы</span></a>
                </div>
            </div>
        </div>
    </main>
@endsection

