<?php
return [
    'main_photo_max' => ['width'=>1280,'height'=>null],
//    'main_photo_max' => ['width'=>1280,'height'=>720],
    'main_photo_min' => ['width'=>570,'height'=>428],
    'mini_photo' => ['width'=>500,'height'=>375],
    'slider_photo' => ['width'=>1920,'height'=>null],
//    'slider_photo' => ['width'=>19200,'height'=>1080],
];
